<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Thu Jan 16 2020 10:06:47 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="5d63f9501fb96636dd5d02ca" data-wf-site="5cea4eba7a0da81f0863712a">
<head>
  <meta charset="utf-8">
  <title>Dialogschmiede AGB</title>
  <meta content="Die allgemeinen Geschäftsbedingungen der Dialogschmiede GmbH. Von Vertragsabschlüssen über Gewährleistungen bis hin zum Ideenschutz." name="description">
  <meta content="Dialogschmiede Agentur" property="og:title">
  <meta content="Die allgemeinen Geschäftsbedingungen der Dialogschmiede GmbH. Von Vertragsabschlüssen über Gewährleistungen bis hin zum Ideenschutz." property="og:description">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/dialogschmiede.webflow.css" rel="stylesheet" type="text/css">
  <!--script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></!--script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]  }});</script>-->
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="images/webclip.png" rel="apple-touch-icon">
<?php include "header.inc.php"; ?></head>
<body class="body">
  <div data-ix="preloader" class="page-loader"></div>
  <div data-ix="page-wrapper-load" class="page-wrapper">
    <div class="contact-popup">
      <div class="popup-wrapper">
        <div class="contact-popup-window w-form">
          <a href="#" data-ix="hide-contact-popup" class="minimize-icon w-inline-block">
            <div class="minimize-icon-line"></div>
          </a>
          <h3>Kontaktieren Sie uns</h3>
          <form id="wf-form-Contact-Popup-Form" name="wf-form-Contact-Popup-Form" data-name="Contact Popup Form" class="contact-window-form"><input type="text" id="Name-3" name="Name" data-name="Name" placeholder="Name" maxlength="256" class="input w-input"><input type="email" id="Email-3" name="Email" data-name="Email" placeholder="E-Mail Adresse" maxlength="256" required="" class="input w-input"><textarea id="Message" name="Message" placeholder="Wie können wir Ihnen helfen?" maxlength="5000" required="" data-name="Message" class="input text-area w-input"></textarea><input type="submit" value="Nachricht senden" data-wait="Please wait..." class="button form-button w-button"></form>
          <div class="form-success window-success w-form-done">
            <div class="text-block-16">Vielen Dank! Wir haben Ihre Nachricht erhalten und melden uns bald.</div>
          </div>
          <div class="form-error window-error w-form-fail">
            <div class="text-block-15">Oops! Da ist etwas schiefgelaufen. Bitte versuchen Sie es erneut.</div>
          </div>
        </div>
        <div data-ix="hide-contact-popup" class="popup-overlay"></div>
      </div>
    </div>
    <div data-collapse="medium" data-animation="default" data-duration="400" data-ix="nav-bar" class="nav-bar w-nav">
      <div class="wrapper w-container"><a href="/" data-ix="logo" class="logo-link w-nav-brand"><img src="images/dialogschmiede_logo_1.png" width="180" srcset="images/dialogschmiede_logo_1-p-500.png 500w, images/dialogschmiede_logo_1.png 600w" sizes="(max-width: 479px) 100vw, 180px" alt=""></a>
        <nav role="navigation" class="nav-menu w-nav-menu"><a href="portfolio" class="nav-link">Cases</a><a href="news" class="nav-link">News</a><a href="team" class="nav-link">Team</a><a href="kontakt" class="nav-link">Kontakt</a></nav>
        <div class="menu-button white-menu-button w-nav-button">
          <div class="w-icon-nav-menu" style="color:black"></div>
        </div>
      </div>
    </div>
    <div class="section textblock">
      <div class="section-impressum textblock">
        <h3 class="section-header-impressum"><strong>AGB</strong></h3>
        <div class="divider left"></div>
        <p class="grey-text">         
          <strong>ALLGEMEINE GESCHÄFTSBEDINGUNGEN der Dialogschmiede GmbH </strong><br>
            Firmenbuchnummer FN 295656p 
            <br>
            Ungargasse 64-66/1/110, 1030 Wien, Österreich 
            <br>
            Telefon: +43 1 890 64 01-0 
            <br>
            Fax: +43 1 890 64 01-99 
            <br>
            office@dialogschmiede.com 
            <br>
            <br>  

            (Empfohlen vom Fachverband Werbung und Marktkommunikation der Wirtschaftskammer Österreich) 
            <br>
            Stand: 1.3.2020 
            <br>
            <br>
            <br>
            <strong>1. Geltung, Vertragsabschluss </strong>
            <br>

            1.1 Die Dialogschmiede GmbH (im Folgenden „Agentur“) erbringt ihre Leistungen ausschließlich auf der Grundlage der nachfolgenden Allgemeinen Geschäftsbedingungen (AGB). Diese gelten für alle Rechtsbeziehungen zwischen der Agentur und dem Kunden, selbst wenn nicht ausdrücklich auf sie Bezug genommen wird. Die AGB sind ausschließlich für Rechtsbeziehung mit Unternehmern anwendbar, sohin B2B. 
            <br>
            1.2 Maßgeblich ist jeweils die zum Zeitpunkt des Vertragsschlusses gültige Fassung. Abweichungen von diesen sowie sonstige ergänzende Vereinbarungen mit dem Kunden sind nur wirksam, wenn sie von der Agentur schriftlich bestätigt werden. 
            <br>
            1.3 Allfällige Geschäftsbedingungen des Kunden werden, selbst bei Kenntnis, nicht akzeptiert, sofern nicht im Einzelfall ausdrücklich und schriftlich anderes vereinbart wird. AGB des Kunden widerspricht die Agentur ausdrücklich. Eines weiteren Widerspruchs gegen AGB des Kunden durch die Agentur bedarf es nicht. 
            <br>
            1.4 Änderungen der AGB werden dem Kunden bekannt gegeben und gelten als vereinbart, wenn der Kunde den geänderten AGB nicht schriftlich binnen 14 Tagen widerspricht; auf die Bedeutung des Schweigens sowie auf die konkret geänderten Klauseln wird der Kunde in der Verständigung ausdrücklich hingewiesen. Diese Zustimmungsfiktion gilt nicht für die Änderung wesentlicher Leistungsinhalte und Entgelte. 
            <br>
            1.5 Sollten einzelne Bestimmungen dieser Allgemeinen Geschäftsbedingungen unwirksam sein, so berührt dies die Verbindlichkeit der übrigen Bestimmungen und der unter ihrer Zugrundelegung geschlossenen Verträge nicht. Die unwirksame Bestimmung ist durch eine wirksame, die dem Sinn und Zweck am nächsten kommt, zu ersetzen. 
            <br>
            1.6 Die Angebote der Agentur sind freibleibend und unverbindlich. 
            <br>
            <br>

            <strong>2. Social Media Kanäle</strong> 
            <br>
            Die Agentur weist den Kunden vor Auftragserteilung ausdrücklich darauf hin, dass die Anbieter von „Social-Media-Kanälen“ (z.B. Facebook, im Folgenden kurz: Anbieter) es sich in ihren Nutzungsbedingungen vorbehalten, Werbeanzeigen und -auftritte aus beliebigen Grund abzulehnen oder zu entfernen. Die Anbieter sind demnach nicht verpflichtet, Inhalte und Informationen an die Nutzer weiterzuleiten. Es besteht daher das von der Agentur nicht kalkulierbare Risiko, dass Werbeanzeigen und -auftritte grundlos entfernt werden. Im Fall einer Beschwerde eines anderen Nutzers wird zwar von den Anbietern die Möglichkeit einer Gegendarstellung eingeräumt, doch erfolgt auch in diesem Fall eine sofortige Entfernung der Inhalte. Die Wiedererlangung des ursprünglichen, rechtmäßigen Zustandes kann in diesem Fall einige Zeit in Anspruch nehmen. Die Agentur arbeitet auf der Grundlage dieser Nutzungsbedingungen der Anbieter, auf die sie keinen Einfluss hat, und legt diese auch dem Auftrag des Kunden zu Grunde. Ausdrücklich anerkennt der Kunde mit der Auftragserteilung, dass diese Nutzungsbedingungen die Rechte und Pflichten eines allfälligen Vertragsverhältnisses (mit-)bestimmen. Die Agentur beabsichtigt, den Auftrag des Kunden nach bestem Wissen und Gewissen auszuführen und die Richtlinien von „Social Media Kanälen“ einzuhalten. Aufgrund der derzeit gültigen Nutzungsbedingungen und der einfachen Möglichkeit jedes Nutzers, Rechtsverletzungen zu behaupten und so eine Entfernung der Inhalte zu erreichen, kann die Agentur aber nicht dafür einstehen, dass die beauftragte Kampagne auch jederzeit abrufbar ist. 
            <br>
            <br>

            <strong>3. Konzept- und Ideenschutz </strong>
            <br>
            Hat der potentielle Kunde die Agentur vorab bereits eingeladen, ein Konzept zu erstellen, und kommt die Agentur dieser Einladung noch vor Abschluss des Hauptvertrages nach, so gilt nachstehende Regelung: 
            <br>
            3.1 Bereits durch die Einladung und die Annahme der Einladung durch die Agentur treten der potentielle Kunde und die Agentur in ein Vertragsverhältnis („Pitching-Vertrag“). Auch diesem Vertrag liegen die AGB zu Grunde.   
            <br>
            3.2 Der potentielle Kunde anerkennt, dass die Agentur bereits mit der Konzepterarbeitung kostenintensive Vorleistungen erbringt, obwohl er selbst noch keine Leistungspflichten übernommen hat.  
            <br>
            3.3 Das Konzept untersteht in seinen sprachlichen und grafischen Teilen, soweit diese Werkhöhe erreichen, dem Schutz des Urheberrechtsgesetzes. Eine Nutzung und Bearbeitung dieser Teile ohne Zustimmung der Agentur ist dem potentiellen Kunden schon auf Grund des Urheberrechtsgesetzes nicht gestattet. 
            <br>
            3.4 Das Konzept enthält darüber hinaus werberelevante Ideen, die keine Werkhöhe erreichen und damit nicht den Schutz des Urheberrechtsgesetzes genießen. Diese Ideen stehen am Anfang jedes Schaffensprozesses und können als zündender Funke alles später Hervorgebrachten und somit als Ursprung von Vermarktungsstrategie definiert werden. Daher sind jene Elemente des Konzeptes geschützt, die eigenartig sind und der Vermarktungsstrategie ihre charakteristische Prägung geben. Als Idee im Sinne dieser Vereinbarung werden insbesondere Werbeschlagwörter, Werbetexte, Grafiken und Illustrationen, Werbemittel usw. angesehen, auch wenn sie keine Werkhöhe erreichen. 
            <br>
            3.5 Der potentielle Kunde verpflichtet sich, es zu unterlassen, diese von der Agentur im Rahmen des Konzeptes präsentierten kreativen Werbeideen außerhalb des Korrektivs eines später abzuschließenden Hauptvertrages wirtschaftlich zu verwerten bzw. verwerten zu lassen oder zu nutzen bzw. nutzen zu lassen. 
            <br>
            3.6 Soferne der potentielle Kunde der Meinung ist, dass ihm von der Agentur Ideen präsentiert wurden, auf die er bereits vor der Präsentation gekommen ist, so hat er dies der Agentur binnen 14 Tagen nach dem Tag der Präsentation per E-Mail unter Anführung von Beweismitteln, die eine zeitliche Zuordnung erlauben, bekannt zu geben.  
            <br>
            3.7 Im gegenteiligen Fall gehen die Vertragsparteien davon aus, dass die Agentur dem potentiellen Kunden eine für ihn neue Idee präsentiert hat. Wird die Idee vom Kunden verwendet, so ist davon auszugehen, dass die Agentur dabei verdienstlich wurde.     
            <br>
            3.8 Der potentielle Kunde kann sich von seinen Verpflichtungen aus diesem Punkt durch Zahlung einer angemessenen Entschädigung zuzüglich 20 % Umsatzsteuer befreien. Die Befreiung tritt erst nach vollständigem Eingang der Zahlung der Entschädigung bei der Agentur ein.  
            <br>
            <br>

            <strong>4. Leistungsumfang, Auftragsabwicklung und Mitwirkungspflichten des Kunden</strong> 
            <br>
            4.1 Der Umfang der zu erbringenden Leistungen ergibt sich aus der Leistungsbeschreibung im Agenturvertrag oder einer allfälligen Auftragsbestätigung durch die Agentur, sowie dem allfälligen Briefingprotokoll („Angebotsunterlagen“). Nachträgliche Änderungen des Leistungsinhaltes bedürfen der schriftlichen Bestätigung durch die Agentur. Innerhalb des vom Kunden vorgegeben Rahmens besteht bei der Erfüllung des Auftrages Gestaltungsfreiheit der Agentur. 
            <br>
            4.2 Alle Leistungen der Agentur (insbesondere alle Vorentwürfe, Skizzen, Reinzeichnungen, Bürstenabzüge, Blaupausen, Kopien, Farbabdrucke und elektronische Dateien) sind vom Kunden zu überprüfen und von ihm binnen drei Werktagen ab Eingang beim Kunden freizugeben. Nach Verstreichen dieser Frist ohne Rückmeldung des Kunden gelten sie als vom Kunden genehmigt.  
            <br>
            4.3 Der Kunde wird der Agentur zeitgerecht und vollständig alle Informationen und Unterlagen zugänglich machen, die für die Erbringung der Leistung erforderlich sind. Er wird sie von allen Umständen informieren, die für die Durchführung des Auftrages von Bedeutung sind, auch wenn diese erst während der Durchführung des Auftrages bekannt werden. Der Kunde trägt den Aufwand, der dadurch entsteht, dass Arbeiten infolge seiner unrichtigen, unvollständigen oder nachträglich geänderten Angaben von der Agentur wiederholt werden müssen oder verzögert werden. 
            <br>
            4.4 Der Kunde ist weiters verpflichtet, die für die Durchführung des Auftrages zur Verfügung gestellten Unterlagen (Fotos, Logos etc.) auf allfällige Urheber-, Marken-, Kennzeichenrechte oder sonstige Rechte Dritter zu prüfen (Rechteclearing) und garantiert, dass die Unterlagen frei von Rechten Dritter sind und daher für den angestrebten Zweck eingesetzt werden können. Die Agentur haftet im Falle bloß leichter Fahrlässigkeit oder nach Erfüllung ihrer Warnpflicht – jedenfalls im Innenverhältnis zum Kunden – nicht wegen einer Verletzung derartiger Rechte Dritter durch zur Verfügung gestellte Unterlagen. Wird die Agentur wegen einer solchen Rechtsverletzung von einem Dritten in Anspruch genommen, so hält der Kunde die Agentur schad- und klaglos; er hat ihr sämtliche Nachteile zu ersetzen, die ihr durch eine Inanspruchnahme Dritter entstehen, insbesondere die Kosten einer angemessenen rechtlichen Vertretung. Der Kunde verpflichtet sich, die Agentur bei der Abwehr von allfälligen Ansprüchen Dritter zu unterstützen. Der Kunde stellt der Agentur hierfür unaufgefordert sämtliche Unterlagen zur Verfügung. 
            <br>
            <br>

            <strong>5. Fremdleistungen / Beauftragung Dritter </strong>
            <br>
            5.1 Die Agentur ist nach freiem Ermessen berechtigt, die Leistung selbst auszuführen, sich bei der Erbringung von vertragsgegenständlichen Leistungen sachkundiger Dritter als Erfüllungsgehilfen zu bedienen und/oder derartige Leistungen zu substituieren („Fremdleistung“). 
            <br>
            5.2 Die Beauftragung von Dritten im Rahmen einer Fremdleistung erfolgt entweder im eigenen Namen oder im Namen des Kunden, letztere nach vorheriger Information an den Kunden. Die Agentur wird diesen Dritten sorgfältig auswählen und darauf achten, dass dieser über die erforderliche fachliche Qualifikation verfügt. 
            <br>
            5.3 In Verpflichtungen gegenüber Dritten, die dem Kunden namhaft gemacht wurden und die über die Vertragslaufzeit hinausgehen, hat der Kunde einzutreten. Das gilt ausdrücklich auch im Falle einer Kündigung des Agenturvertrages aus wichtigem Grund. 
            <br>
            <br>

            <strong>6. Termine </strong>
            <br>
            6.1 Angegebene Liefer- oder Leistungsfristen gelten, sofern nicht ausdrücklich als verbindlich vereinbart, nur als annähernd und unverbindlich. Verbindliche Terminabsprachen sind schriftlich festzuhalten bzw. von der Agentur schriftlich zu bestätigen.  
            <br>
            6.2 Verzögert sich die Lieferung/Leistung der Agentur aus Gründen, die sie nicht zu vertreten hat, wie z.B. Ereignisse höherer Gewalt und andere unvorhersehbare, mit zumutbaren Mitteln nicht abwendbare Ereignisse, ruhen die Leistungsverpflichtungen für die Dauer und im Umfang des Hindernisses und verlängern sich die Fristen entsprechend. Sofern solche Verzögerungen mehr als zwei Monate andauern, sind der Kunde und die Agentur berechtigt, vom Vertrag zurückzutreten.  
            <br>
            6.3 Befindet sich die Agentur in Verzug, so kann der Kunde vom Vertrag nur zurücktreten, nachdem er der Agentur schriftlich eine angemessene Nachfrist von zumindest 14 Tagen gesetzt hat und diese fruchtlos verstrichen ist. Schadenersatzansprüche des Kunden wegen Nichterfüllung oder Verzug sind ausgeschlossen, ausgenommen bei Nachweis von Vorsatz oder grober Fahrlässigkeit. 
            <br>
            <br>

            <strong>7. Vorzeitige Auflösung</strong> 
            <br>
            7.1 Die Agentur ist berechtigt, den Vertrag aus wichtigen Gründen mit sofortiger Wirkung aufzulösen. Ein wichtiger Grund liegt insbesondere vor, wenn 
            <br>
            a) die Ausführung der Leistung aus Gründen, die der Kunde zu vertreten hat, unmöglich wird oder trotz Setzung einer Nachfrist von 14 Tagen weiter verzögert wird;  
            <br>
            b) der Kunde fortgesetzt, trotz schriftlicher Abmahnung mit einer Nachfristsetzung von 14 Tagen, gegen wesentliche Verpflichtungen aus diesem Vertrag, wie z.B. Zahlung eines fällig gestellten Betrages oder Mitwirkungspflichten, verstößt. 
            <br>
            c) berechtigte Bedenken hinsichtlich der Bonität des Kunden bestehen und dieser auf Begehren der Agentur weder Vorauszahlungen leistet noch vor Leistung der Agentur eine taugliche Sicherheit leistet; 
            <br>
            7.2 Der Kunde ist berechtigt, den Vertrag aus wichtigen Gründen ohne Nachfristsetzung aufzulösen. Ein wichtiger Grund liegt insbesondere dann vor, wenn die Agentur fortgesetzt, trotz schriftlicher Abmahnung mit einer angemessenen Nachfrist von zumindest 14 Tagen zur Behebung des Vertragsverstoßes gegen wesentliche Bestimmungen aus diesem Vertrag verstößt. 
            <br>
            <br>
            <strong>8. Honorar </strong>
            <br>
            8.1 Wenn nichts anderes vereinbart ist, entsteht der Honoraranspruch der Agentur für jede einzelne Leistung, sobald diese erbracht wurde. Die Agentur ist berechtigt, zur Deckung ihres Aufwandes Vorschüsse zu verlangen. Ab einem Auftragsvolumen mit einem (jährlichen) Budget von € 10.000,– oder solchen, die sich über einen längeren Zeitraum erstrecken ist die Agentur berechtigt, Zwischenabrechnungen bzw. Vorausrechnungen zu erstellen oder Akontozahlungen abzurufen. 
            <br>
            8.2 Das Honorar versteht sich als Netto-Honorar zuzüglich der Umsatzsteuer in gesetzlicher Höhe. Mangels Vereinbarung im Einzelfall hat die Agentur für die erbrachten Leistungen und die Überlassung der urheber- und kennzeichenrechtlichen Nutzungsrechte Anspruch auf Honorar in der marktüblichen Höhe. 
            <br>
            8.3 Alle Leistungen der Agentur, die nicht ausdrücklich durch das vereinbarte Honorar abgegolten sind, werden gesondert entlohnt. Alle der Agentur erwachsenden Barauslagen sind vom Kunden zu ersetzen. 
            <br>
            8.4 Kostenvoranschläge der Agentur sind unverbindlich. Wenn abzusehen ist, dass die tatsächlichen Kosten die von der Agentur schriftlich veranschlagten um mehr als 15 % übersteigen, wird die Agentur den Kunden auf die höheren Kosten hinweisen. Die Kostenüberschreitung gilt als vom Kunden genehmigt, wenn der Kunde nicht binnen drei Werktagen nach diesem Hinweis schriftlich widerspricht und gleichzeitig kostengünstigere Alternativen bekannt gibt. Handelt es sich um eine Kostenüberschreitung bis 15 % ist eine gesonderte Verständigung nicht erforderlich. Diese Kostenvoranschlagsüberschreitung gilt vom Auftraggeber von vornherein als genehmigt. 
            <br>
            8.5 Wenn der Kunde in Auftrag gegebene Arbeiten ohne Einbindung der Agentur – unbeschadet der laufenden sonstigen Betreuung durch diese – einseitig ändert oder abbricht, hat er der Agentur die bis dahin erbrachten Leistungen entsprechend der Honorarvereinbarung zu vergüten und alle angefallenen Kosten zu erstatten. Sofern der Abbruch nicht durch eine grob fahrlässige oder vorsätzliche Pflichtverletzung der Agentur begründet ist, hat der Kunde der Agentur darüber hinaus das gesamte für diesen Auftrag vereinbarte Honorar (Provision) zu erstatten, wobei die Anrechnungsvergütung des § 1168 AGBG ausgeschlossen wird. Weiters ist die Agentur bezüglich allfälliger Ansprüche Dritter, insbesondere von Auftragnehmern der Agentur, schad- und klaglos zu stellen. Mit der Bezahlung des Entgelts erwirbt der Kunde an bereits erbrachten Arbeiten keinerlei Nutzungsrechte; nicht ausgeführte Konzepte, Entwürfe und sonstige Unterlagen sind vielmehr unverzüglich der Agentur zurückzustellen. 
            <br>
            <br>

            <strong>9. Zahlung, Eigentumsvorbehalt </strong>
            <br>
            9.1 Das Honorar ist sofort mit Rechnungserhalt und ohne Abzug zur Zahlung fällig, sofern nicht im Einzelfall besondere Zahlungsbedingungen schriftlich vereinbart werden. Dies gilt auch für die Weiterverrechnung sämtlicher Barauslagen und sonstiger Aufwendungen. Die von der Agentur gelieferte Ware bleibt bis zur vollständigen Bezahlung des Entgelts einschließlich aller Nebenverbindlichkeiten im Eigentum der Agentur. 
            <br>
            9.2 Bei Zahlungsverzug des Kunden gelten die gesetzlichen Verzugszinsen in der für Unternehmergeschäfte geltenden Höhe. Weiters verpflichtet sich der Kunde für den Fall des Zahlungsverzugs, der Agentur die entstehenden Mahn- und Inkassospesen, soweit sie zur zweckentsprechenden Rechtsverfolgung notwendig sind, zu ersetzen. Dies umfasst jedenfalls die Kosten zweier Mahnschreiben in marktüblicher Höhe von derzeit zumindest € 20,– je Mahnung sowie eines Mahnschreibens eines mit der Eintreibung beauftragten Rechtsanwalts. Die Geltendmachung weitergehender Rechte und Forderungen bleibt davon unberührt.  
            <br>
            9.3 Im Falle des Zahlungsverzuges des Kunden kann die Agentur sämtliche, im Rahmen anderer mit dem Kunden abgeschlossener Verträge, erbrachten Leistungen und Teilleistungen sofort fällig stellen.  
            <br>
            9.4 Weiters ist die Agentur nicht verpflichtet, weitere Leistungen bis zur Begleichung des aushaftenden Betrages zu erbringen (Zurückbehaltungsrecht). Die Verpflichtung zur Entgeltzahlung bleibt davon unberührt.  
            <br>
            9.5 Wurde die Bezahlung in Raten vereinbart, so behält sich die Agentur für den Fall der nicht fristgerechten Zahlung von Teilbeträgen oder Nebenforderungen das Recht vor, die sofortige Bezahlung der gesamten noch offenen Schuld zu fordern (Terminverlust). 
            <br>
            9.6 Der Kunde ist nicht berechtigt, mit eigenen Forderungen gegen Forderungen der Agentur aufzurechnen, außer die Forderung des Kunden wurde von der Agentur schriftlich anerkannt oder gerichtlich festgestellt.  
            <br>
            <br>

            <strong>10. Eigentumsrecht und Urheberrecht</strong> 
            <br>
            10.1 Alle Leistungen der Agentur, einschließlich jener aus Präsentationen (z.B. Anregungen, Ideen, Skizzen, Vorentwürfe, Skribbles, Reinzeichnungen, Konzepte, Negative, Dias), auch einzelne Teile daraus, bleiben ebenso wie die einzelnen Werkstücke und Entwurfsoriginale im Eigentum der Agentur und können von der Agentur jederzeit – insbesondere bei Beendigung des Vertragsverhältnisses – zurückverlangt werden. Der Kunde erwirbt durch Zahlung des Honorars das Recht der Nutzung für den vereinbarten Verwendungszweck. Mangels anderslautender Vereinbarung darf der Kunde die Leistungen der Agentur jedoch ausschließlich in Österreich nutzen. Der Erwerb von Nutzungs- und Verwertungsrechten an Leistungen der Agentur setzt in jedem Fall die vollständige Bezahlung der von der Agentur dafür in Rechnung gestellten Honorare voraus. Nutzt der Kunde bereits vor diesem Zeitpunkt die Leistungen der Agentur, so beruht diese Nutzung auf einem jederzeit widerrufbaren Leihverhältnis.  
            <br>
            10.2 Änderungen bzw. Bearbeitungen von Leistungen der Agentur, wie insbesondere deren Weiterentwicklung durch den Kunden oder durch für diesen tätige Dritte, sind nur mit ausdrücklicher Zustimmung der Agentur und – soweit die Leistungen urheberrechtlich geschützt sind – des Urhebers zulässig. Die Herausgabe aller sogen. „offenen Dateien“ wird damit ausdrücklich nicht Vertragsbestandteil. Die Agentur ist nicht zur Herausgabe verpflichtet. D.h. ohne vertragliche Abtretung der Nutzungsrechte auch für „elektronische Arbeiten“ hat der Auftraggeber keinen Rechtsanspruch darauf.  
            <br>
            10.3 Für die Nutzung von Leistungen der Agentur, die über den ursprünglich vereinbarten Zweck und Nutzungsumfang hinausgeht, ist – unabhängig davon, ob diese Leistung urheberrechtlich geschützt ist – die Zustimmung der Agentur erforderlich. Dafür steht der Agentur und dem Urheber eine gesonderte angemessene Vergütung zu. 
            <br>
            10.4 Für die Nutzung von Leistungen der Agentur bzw. von Werbemitteln, für die die Agentur konzeptionelle oder gestalterische Vorlagen erarbeitet hat, ist nach Ablauf des Agenturvertrages unabhängig davon, ob diese Leistung urheberrechtlich geschützt ist oder nicht, ebenfalls die Zustimmung der Agentur notwendig. 
            <br>
            10.5 Für Nutzungen gemäß Abs 4. steht der Agentur im 1. Jahr nach Vertragsende ein Anspruch auf die volle im abgelaufenen Vertrag vereinbarte Agenturvergütung zu. Im 2. bzw. 3. Jahr nach Ablauf des Vertrages nur mehr die Hälfte bzw. ein Viertel der im Vertrag vereinbarten Vergütung. Ab dem 4. Jahr nach Vertragsende ist keine Agenturvergütung mehr zu zahlen. 
            <br>
            10.6 Der Kunde haftet der Agentur für jede widerrechtliche Nutzung in doppelter Höhe des für diese Nutzung angemessenen Honorars.  
            <br>
            <br>

            <strong>11. Kennzeichnung</strong> 
            <br>
            11.1 Die Agentur ist berechtigt, auf allen Werbemitteln und bei allen Werbemaßnahmen auf die Agentur und allenfalls auf den Urheber hinzuweisen, ohne dass dem Kunden dafür ein Entgeltanspruch zusteht. 
            <br>
            11.2 Die Agentur ist vorbehaltlich des jederzeit möglichen, schriftlichen Widerrufs des Kunden dazu berechtigt, auf eigenen Werbeträgern und insbesondere auf ihrer Internet-Website mit Namen und Firmenlogo auf die zum Kunden bestehende oder vormalige Geschäftsbeziehung hinzuweisen (Referenzhinweis). 
            <br>
            <br>

            <strong>12. Gewährleistung </strong> 
            <br>
            12.1 Der Kunde hat allfällige Mängel unverzüglich, jedenfalls innerhalb von acht Tagen nach Lieferung/Leistung durch die Agentur, verdeckte Mängel innerhalb von acht Tagen nach Erkennen derselben, schriftlich unter Beschreibung des Mangels anzuzeigen; andernfalls gilt die Leistung als genehmigt. In diesem Fall ist die Geltendmachung von Gewährleistungs- und Schadenersatzansprüchen sowie das Recht auf Irrtumsanfechtung aufgrund von Mängeln ausgeschlossen. 
            <br>
            12.2 Im Fall berechtigter und rechtzeitiger Mängelrüge steht dem Kunden das Recht auf Verbesserung oder Austausch der Lieferung/Leistung durch die Agentur zu. Die Agentur wird die Mängel in angemessener Frist beheben, wobei der Kunde der Agentur alle zur Untersuchung und Mängelbehebung erforderlichen Maßnahmen ermöglicht. Die Agentur ist berechtigt, die Verbesserung der Leistung zu verweigern, wenn diese unmöglich oder für die Agentur mit einem unverhältnismäßig hohen Aufwand verbunden ist. In diesem Fall stehen dem Kunden die gesetzlichen Wandlungs- oder Minderungsrechte zu. Im Fall der Verbesserung obliegt es dem Auftraggeber die Übermittlung der mangelhaften (körperlichen) Sache auf seine Kosten durchzuführen. 
            <br>
            12.3 Es obliegt auch dem Auftraggeber, die Überprüfung der Leistung auf ihre rechtliche, insbesondere wettbewerbs-, marken-, urheber- und verwaltungsrechtliche Zulässigkeit durchzuführen. Die Agentur ist nur zu einer Grobprüfung der rechtlichen Zulässigkeit verpflichtet. Die Agentur haftet im Falle leichter Fahrlässigkeit oder nach Erfüllung einer allfälligen Warnpflicht gegenüber dem Kunden nicht für die rechtliche Zulässigkeit von Inhalten, wenn diese vom Kunden vorgegeben oder genehmigt wurden. 
            <br>
            12.4 Die Gewährleistungsfrist beträgt sechs Monate ab Lieferung/Leistung. Das Recht zum Regress gegenüber der Agentur gemäß § 933b Abs 1 AGBG erlischt ein Jahr nach Lieferung/Leistung. Der Kunde ist nicht berechtigt, Zahlungen wegen Bemängelungen zurückzuhalten. Die Vermutungsregelung des § 924 AGBG wird ausgeschlossen.  
            <br>
            <br>

            <strong>13. Abwerbeverbot von Mitarbeitern </strong>
            <br>
            13.1 Der Kunde verpflichtet sich, während sowie zwei Jahre nach Beendigung eines Vertrags bzw. eines Auftrages das vom Auftragnehmer zur Leistungserbringung eingesetzte Personal direkt oder indirekt nicht abzuwerben oder bei sich oder einem Tochterbetrieb anzustellen. Für jeden Fall der Zuwiderhandlung gegen diese Bestimmung, zahlt die verstoßende Partei an die andere Partei eine nicht dem richterlichen Mäßigungsrecht unterliegende Konventional- bzw. Vertragsstrafe in Höhe von zwei Bruttojahresgehältern (einschl. Prämien, Tantiemen) des betreffenden Mitarbeiters, der unter Verstoß gegen die Verpflichtung von der betreffenden Partei abgeworben wird, wobei zur Berechnung der Vertragsstrafe das Bruttojahresgehalt des betreffenden Mitarbeiters maßgeblich ist, das er im Jahr vor Verwirkung der Vertragsstrafe bezogen hat. 
            <br>
            <br>

            <strong>14. Haftung und Produkthaftung</strong> 
            <br>
            14.1 In Fällen leichter Fahrlässigkeit ist eine Haftung der Agentur und die ihrer Angestellten, Auftragnehmer oder sonstigen Erfüllungsgehilfen („Leute“) für Sach- oder Vermögensschäden des Kunden ausgeschlossen, gleichgültig ob es sich um unmittelbare oder mittelbare Schäden, entgangenen Gewinn oder Mangelfolgeschäden, Schäden wegen Verzugs, Unmöglichkeit, positiver Forderungsverletzung, Verschuldens bei Vertragsabschluss, wegen mangelhafter oder unvollständiger Leistung handelt. Das Vorliegen von grober Fahrlässigkeit hat der Geschädigte zu beweisen. Soweit die Haftung der Agentur ausgeschlossen oder beschränkt ist, gilt dies auch für die persönliche Haftung ihrer „Leute“.  
            <br>
            14.2 Jegliche Haftung der Agentur für Ansprüche, die auf Grund der von der Agentur erbrachten Leistung (z.B. Werbemaßnahme) gegen den Kunden erhoben werden, wird ausdrücklich ausgeschlossen, wenn die Agentur ihrer Hinweispflicht nachgekommen ist oder eine solche für sie nicht erkennbar war, wobei leichte Fahrlässigkeit nicht schadet. Insbesondere haftet die Agentur nicht für Prozesskosten, eigene Anwaltskosten des Kunden oder Kosten von Urteilsveröffentlichungen sowie für allfällige Schadenersatzforderungen oder sonstige Ansprüche Dritter; der Kunde hat die Agentur diesbezüglich schad- und klaglos zu halten. 
            <br>
            14.3 Schadensersatzansprüche des Kunden verfallen in sechs Monaten ab Kenntnis des Schadens; jedenfalls aber nach drei Jahren ab der Verletzungshandlung der Agentur. Schadenersatzansprüche sind der Höhe nach mit dem Netto-Auftragswert begrenzt. 
            <br>
            <br>

            <strong>15. Anzuwendendes Recht</strong> 
            <br>
            Der Vertrag und alle daraus abgeleiteten wechselseitigen Rechte und Pflichten sowie Ansprüche zwischen der Agentur und dem Kunden unterliegen dem österreichischen materiellen Recht unter Ausschluss seiner Verweisungsnormen und unter Ausschluss des UN-Kaufrechts. 
            <br>
            <br>

            <strong>16. Erfüllungsort und Gerichtsstand</strong> 
            <br>
            16.1 Erfüllungsort ist der Sitz der Agentur. Bei Versand geht die Gefahr auf den Kunden über, sobald die Agentur die Ware dem von ihr gewählten Beförderungsunternehmen übergeben hat. 
            <br>
            16.2 Als Gerichtsstand für alle sich zwischen der Agentur und dem Kunden ergebenden Rechtsstreitigkeiten im Zusammenhang mit diesem Vertragsverhältnis wird das für den Sitz der Agentur sachlich zuständige Gericht (Handelsgericht Wien) vereinbart. Ungeachtet dessen ist die Agentur berechtigt, den Kunden an seinem allgemeinen Gerichtsstand zu klagen. 
            <br>
            16.3 Soweit in diesem Vertrag auf natürliche Personen bezogene Bezeichnungen nur in männlicher Form angeführt sind, beziehen sie sich auf Frauen und Männer in gleicher Weise. Bei der Anwendung der Bezeichnung auf bestimmte natürliche Personen ist die jeweils geschlechtsspezifische Form zu verwenden. 
            <br>
            <br>
</p>
            <h3>Ergänzende Geschäftsbedingungen für Adressenvermittlung </h3>
            <p class="grey-text">     
            <br>
            Die nachfolgenden Bedingungen gelten ergänzend zu den Allgemeinen Geschäftsbedingungen für den Fall, dass die Agentur einem Kunden (Mieter) Adressenbestände eines Adresseneigentümers (Vermieter) vermittelt. 
            <br>
            I. Die Agentur ist nur Makler (List Broker) des Vermieters und kann vom Mieter wegen unrichtiger Adressen oder sonstiger Mängel des Anschriftenmaterials nicht in Anspruch genommen werden. Die Agentur übernimmt daher auch keinerlei Gewähr für die Richtigkeit der Angaben des Vermieters. 
            <br>
            II. Die von der Agentur im Namen des Vermieters abgegebenen Angebote sind freibleibend und bedürfen der schriftlichen Bestätigung durch den Vermieter. Dieser kann die Annahme von Aufträgen, die die Agentur im Namen des Mieters erteilen, ohne Angabe von Gründen ablehnen oder von der Anerkennung zusätzlicher Bedingungen, insbesondere von der Vorlage eines Musterstücks des Werbematerials, mit dem die Adressen bearbeitet werden sollen, abhängig machen. Mit seiner Genehmigung einer Adressennutzung für eine Testaussendung verzichtet der Vermieter auf sein Recht zur Ablehnung für eine gleiche zeitnahe Werbeaussendung aller bestellter Adressen. Mieter und Vermieter anerkennen, dass der Mietvertrag allein zwischen dem Vermieter und dem Mieter zustande kommt. Die Agentur kann als Makler aus dem zwischen Vermieter und Mieter geschlossenen Vertrag unbeschadet ihrer Inkassoberechtigung für den Vermieter nicht in Anspruch genommen werden. 
            <br>
            III. An den Adressen des Vermieters besteht der Datenbankurheberrechtsschutz gem. § 76c ff UrhG, sie bleiben Eigentum des Vermieters und werden dem Mieter nur zur einmaligen Verwendung für eigene Werbung im vereinbarten Umfang vermietet. Will der Mieter die Adressen mehrfach oder unbeschränkt nutzen, so bedarf es hierzu einer gesonderten Dauernutzungsvereinbarung mit dem Vermieter. Zum Schutz gegen unbefugte Verwendung sind in die Adressenkollektionen Kontrolladressen eingearbeitet. Zum Nachweis des Missbrauchs genügt die Vorlage einer Kontrolladresse. Anschriften von Personen, die auf Werbung des Mieters bestellen oder Angebote anfordern, unterliegen in der weiteren Nutzung durch den Mieter keiner Beschränkung. Dies gilt jedoch nicht für die Anschriften von Teilnehmern an Gewinnspielen, Preisausschreiben oder gleichzusetzenden Veranstaltungen. Eine Verarbeitung und Nutzung der vermieteten Adressen darf nur unter Beachtung der Vorschriften des DSG 2000, der DSGVO bzw. der Codes of Conduct (Verhaltensregeln gem. § 6(4) DSG 2000) erfolgen.  
            <br>
            IV. Für den Fall der Zuwiderhandlung gegen die unter Ziffer III erwähnten Nutzungsbeschränkungen hat der Mieter eine Vertragsstrafe zu zahlen in Höhe des 10-fachen Rechnungsbetrages für sämtliche Adressenkollektionen, die zusammen mit der Kollektion geliefert wurden, aus der die vertragswidrig genutzte Anschrift herrührt; die Agentur ist zum Inkasso für den/die Vermieter berechtigt. 
            <br>
            V. Wegen der in den einzelnen Adressengruppen verschiedenen Fluktuation sind Retouren (mit postalischem Unzustellbarkeitsvermerk) unvermeidlich. Eine Vergütung dieser Retouren findet nicht statt. Retourenvergütungen bedürfen einer besonderen Vereinbarung mit dem Vermieter. Der Vermieter übernimmt keine Gewähr dafür, dass der Träger einer Anschrift zum Zeitpunkt des Adresseneinsatzes das ist, wofür er ausgegeben wird oder wofür er sich selbst ausgibt. 
            <br>
            VI. Wegen leicht fahrlässiger Verletzung von Vertragspflichten ist die Haftung des Vermieters beschränkt auf den vertragstypisch vorhersehbaren Schaden. Letzteres gilt nicht, wenn die Verletzung vertragswesentliche Pflichten betrifft. 
            <br>    
        <br></p>
      </div>
    </div>
    <div class="section grey-section no-top-padding no-bottom-padding">
      <div class="wrapper w-container">
        <div class="footer">
          <div class="footer-about"><a href="/" class="footer-logo w-nav-brand"><img src="images/dialogschmiede_logo_1.png" srcset="images/dialogschmiede_logo_1-p-500.png 500w, images/dialogschmiede_logo_1.png 600w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 42vw, (max-width: 991px) 27vw, 20vw" alt=""></a>
            <p class="paragraph-small">Die Dialogschmiede ist Österreichs <br>führende Dialogagentur.</p>
          </div>
          <div class="footer-nav">
            <h5>Navigation</h5><a href="/" class="footer-link">Home</a><a href="portfolio" class="footer-link">Cases</a><a href="news" class="footer-link">News</a><a href="team" class="footer-link">Team</a><a href="kontakt" class="footer-link">Kontakt</a></div>
          <div class="footer-subscribe">
            <!--<h5>Jetzt unseren Newsletter abonnieren:</h5>
            <div class="w-form">
              <form id="wf-form-Subscribe-Form" name="wf-form-Subscribe-Form" data-name="Subscribe Form" class="footer-subscribe-form"><input type="email" id="email-4" name="email-4" data-name="Email 4" placeholder="Ihre E-Mail Adresse" maxlength="256" required="" class="input footer-input w-input"><input type="submit" value="Abonnieren" data-wait="Please wait..." class="button w-button"></form>
              <div class="form-success w-form-done">
                <div>Vielen Dank! Wir haben Ihre Nachricht erhalten und melden uns bald.</div>
              </div>
              <div class="form-error w-form-fail">
                <div>Oops! Da ist etwas schiefgelaufen. Bitte versuchen Sie es erneut.</div>
              </div>
            </div>-->
            <?php include "footer.inc.php"; ?>

        </div>
      </div>
    </div>
  </div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>