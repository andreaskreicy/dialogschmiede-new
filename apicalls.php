<?php

// api access token 5e3ff330cacf70bb089e8612b80b0cf6ad55658cde28054f3ec3dcc5c7705cb4
// $client_id = "19597f5b003b9b7a27a3e9e5a710881423c49e4dcb807c76a632511a256e3355";
// $client_secret = "4c20435d2cfbf00ea070ccd1d617a4ffa319a67c2f15ccb770daba6bd00a35c3";
const WEBFLOW_API_ENDPOINT = 'https://api.webflow.com/';
const WEBFLOW_API_USERAGENT = 'Expertlead Webflow PHP SDK (https://github.com/expertlead/webflow-php-sdk)';
    
       function apicall($call) {
        
        $ch = curl_init();
        $token = '5e3ff330cacf70bb089e8612b80b0cf6ad55658cde28054f3ec3dcc5c7705cb4';

        // curl_setopt($ch, CURLOPT_URL, 'https://api.webflow.com/info');
        $curl = curl_init();
        $options = [
        CURLOPT_URL => WEBFLOW_API_ENDPOINT.$call ,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_USERAGENT => WEBFLOW_API_USERAGENT,
        CURLOPT_HTTPHEADER => [
          "Authorization: Bearer {$token}",
          "accept-version: 1.0.0",
          "Accept: application/json",
          "Content-Type: application/json",
        ],
        CURLOPT_HEADER => true,
        CURLOPT_RETURNTRANSFER => true,
        ];
        // echo WEBFLOW_API_ENDPOINT;
        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);
        curl_close($curl);
        list($headers, $body) = explode("\r\n\r\n", $response, 2);
        return parse($body);
        
        
       }
        

       function parse($response)
        {
            $json = json_decode($response);
            if (isset($json->code) && isset($json->msg)) {
                $error = $json->msg;
                if (isset($json->problems)) {
                    $error .= PHP_EOL . implode(PHP_EOL, $json->problems);
                }
                throw new \Exception($error, $json->code);
            }
            return $json;
        }

    //   collections print_r(apicall('sites/5cea4eba7a0da81f0863712a/collections'));
    // // [{"_id":"5d5fc4f1b6d68d08a702f952", "lastUpdated":"2019-10-09T10:00:43.079Z","createdOn":"2017-07-14T02:29:13.053Z","name":"Works","slug":"works","singularName":"Work"},
    //     {"_id":"5d5fc4f1b6d68d210202f98e","lastUpdated":"2019-08-23T10:50:28.091Z","createdOn":"2017-07-14T02:30:46.288Z","name":"Work Categories","slug":"work-category","singularName":"Work Category"},
    //     {"_id":"5d5fc4f1b6d68d31b102f9a2","lastUpdated":"2019-09-18T07:04:54.580Z","createdOn":"2019-08-22T19:59:35.495Z","name":"Team Members","slug":"team","singularName":"Team Member"},
    //     {"_id":"5d5fc4f1b6d68d454902f966","lastUpdated":"2019-08-23T10:50:28.079Z","createdOn":"2017-07-16T03:38:29.776Z","name":"Blog Posts","slug":"blog","singularName":"Blog Post"},
    //     {"_id":"5d5fc4f1b6d68d478402f927","lastUpdated":"2019-08-23T10:50:27.844Z","createdOn":"2017-07-24T04:19:59.448Z","name":"Blog Tags","slug":"blog-tags","singularName":"Blog Tag"},
    //     {"_id":"5d5fc4f1b6d68d5ec902f97a","lastUpdated":"2019-08-23T10:50:28.086Z","createdOn":"2017-07-16T03:28:27.735Z","name":"Blog Categories","slug":"blog-categories","singularName":"Blog Category"},
    //     {"_id":"5d5fc4f1b6d68d62f602f93e","lastUpdated":"2019-08-23T10:50:28.046Z","createdOn":"2017-07-16T03:23:11.193Z","name":"Blog Authors","slug":"blog-authors","singularName":"Blog Author"}]

    //   rettrieve collections with name and id here..

    //  collection->items  print_r(apicall('collections/5d5fc4f1b6d68d31b102f9a2/items'));
    //  collection single item print_r(apicall('/collections/:collection_id/items/:item_id'));
    ?>