<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Thu Jan 16 2020 10:06:47 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="5d5fd01038c00e186bae4209" data-wf-site="5cea4eba7a0da81f0863712a">
<head>
  <meta charset="utf-8">
  <title>Dialogschmiede Kontakt</title>
  <meta content="Sie finden uns in Wien, Berlin und Zürich. Einfach kontaktieren und Dialog starten: Wir unterstützen Sie gerne mit unserem Expertenwissen." name="description">
  <meta content="Dialogschmiede Agentur" property="og:title">
  <meta content="Sie finden uns in Wien, Berlin und Zürich. Einfach kontaktieren und Dialog starten: Wir unterstützen Sie gerne mit unserem Expertenwissen." property="og:description">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/dialogschmiede.webflow.css" rel="stylesheet" type="text/css">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]  }});</script> -->
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="images/webclip.png" rel="apple-touch-icon">
<?php include "header.inc.php"; ?></head>
<body class="body">
  <div data-ix="preloader" class="page-loader"></div>
  <div data-ix="page-wrapper-load" class="page-wrapper">
    <div data-collapse="medium" data-animation="default" data-duration="400" data-ix="nav-bar" class="nav-bar w-nav">
      <div class="wrapper w-container"><a href="/" data-ix="logo" class="logo-link w-nav-brand"><img src="images/dialogschmiede_logo_1.png" width="180" srcset="images/dialogschmiede_logo_1-p-500.png 500w, images/dialogschmiede_logo_1.png 600w" sizes="(max-width: 479px) 100vw, 180px" alt=""></a>
        <nav role="navigation" class="nav-menu w-nav-menu"><a href="portfolio" class="nav-link nav-link-white">Cases</a><a href="news" class="nav-link nav-link-white">News</a><a href="team" class="nav-link nav-link-white">Team</a><a href="kontakt" class="nav-link nav-link-white w--current">Kontakt</a></nav>
        <div class="menu-button white-menu-button w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div class="page-header-section header-kontakt" alt="Smartphone in der Hand." title="Smartphone in der Hand.">
      <div class="page-header-wrapper">
        <h1 class="page-header">Persönlich und relevant</h1>
      </div>
    </div>
    <div class="section grey-section">
      <div data-ix="slide-left" class="section-header-wrapper">
        <h2 class="section-header-impressum">Kontaktpunkte<br></h2>
        <div class="divider center"></div>
        <p class="grey-text">
          Unsere Standorte in Wien, Berlin und Zürich unterstützen Sie dabei, Ihre Business- und Markenziele zu erreichen.
Als sales-driven Kommunikationsagentur sind wir national und international vertreten, standortübergreifend und agil.
        </p>
      </div>
      <div class="wrapper wrapper-contact w-container">
        <div class="side-feature-media">
          <div class="contact-card">
            <div class="contact-card-info">
              <h5 class="heading-2">Wien</h5>
              <div class="contact-card-line"><img src="images/pin-icon_1pin-icon.png" width="24" alt="">
                <div class="contact-card-line-text">Ungargasse 64-66/1/110<br>1030 Wien</div>
              </div>
              <div class="contact-card-line"><img src="images/phone-icon_1phone-icon.png" width="24" alt="">
                <div class="contact-card-line-text"><a href="tel:+4318906401-0"><span class="text-span-5">+43 1 890 64 01-0</span></a></div>
            </div>  
              <div class="contact-card-line"><img src="images/at-icon_1at-icon.png" width="24" alt="">
                <div class="contact-card-line-text"><a href="mailto:wien@dialogschmiede.com"><span class="text-span-3">wien@dialogschmiede.com</span></a></div>
              </div>
            </div>
          </div>
        </div>
        <div class="side-feature-media">
          <div class="contact-card">
            <div class="contact-card-info">
              <h5 class="heading-3">Berlin</h5>
              <div class="contact-card-line"><img src="images/pin-icon_1pin-icon.png" width="24" alt="">
                <div class="contact-card-line-text">Isoldestraße 2<br>D - 12159 Berlin</div>
              </div>
              <div class="contact-card-line"><img src="images/phone-icon_1phone-icon.png" width="24" alt="">
                <div class="contact-card-line-text"><a href="tel:+493029673755"><span class="text-span-4">+49 30 296 737 55</span></a></div>
              </div>
              <div class="contact-card-line"><img src="images/at-icon_1at-icon.png" width="24" alt="">
                <div class="contact-card-line-text"><a href="mailto:berlin@dialogschmiede.com" class="link">berlin@dialogschmiede.com</a></div>
              </div>
            </div>
          </div>
        </div>
        <div class="side-feature-media">
          <div class="contact-card">
            <div class="contact-card-info">
              <h5 class="heading-4">Zürich</h5>
              <div class="contact-card-line"><img src="images/pin-icon_1pin-icon.png" width="24" alt="">
                <div class="contact-card-line-text">Gutschstrasse 7<br>CH - 6313 Menzingen</div>
              </div>
              <div class="contact-card-line"><img src="images/phone-icon_1phone-icon.png" width="24" alt="">
                <div class="contact-card-line-text"><a href="tel:+41417550259" class="link-2">+41 41 755 02 59</div>
              </div>
              <div class="contact-card-line"><img src="images/at-icon_1at-icon.png" width="24" alt="">
                <div class="contact-card-line-text"><a href="mailto:zuerich@dialogschmiede.com"><span class="text-span-2">zuerich@dialogschmiede.com</span></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="vertical-header"></div>
    </div>
    <div id="Contact-Form" class="section">
      <div class="section-header-wrapper">
        <h2 class="section-header-impressum">Einen Dialog wert</h2>
        <div class="divider center"></div>
        <p class="grey-text">Schreiben Sie uns ein paar Zeilen und wir melden uns so bald wie <br>möglich zurück!</p>
      </div>
      <div class="wrapper w-container">
        <div class="request w-form">
          <form id="wf-form-Contact-Form" name="wf-form-Contact-Form" data-name="Contact Form" class="request-form">
            <div class="request-block-half"><label for="Name" class="form-label">Name:</label><input type="text" class="input w-input" maxlength="256" name="Name" data-name="Name" placeholder="Ihr Name" id="Name" required=""></div>
            <div class="request-block-half"><label for="Email" class="form-label">E-Mail:</label><input type="text" class="input w-input" maxlength="256" name="Email" data-name="Email 3" placeholder="Ihre E-Mail-Adresse" id="Email-3" required=""></div>
            <div class="request-block-full"><label for="Message" class="form-label">Nachricht:</label><textarea id="Message" name="Message" placeholder="Schreiben Sie uns ein paar Zeilen..." maxlength="5000" data-name="Message" class="input text-area big w-input"></textarea></div>
            <p class="grey-text">Durch das Abschicken der Nachricht, stimmen Sie zu, dass Ihre Angaben und Daten zur Beantwortung Ihrer Anfrage elektronisch erhoben und gespeichert werden. Ein Widerruf ist jederzeit per Mail an <a href="mailto:datenschutz@dialogschmiede.com">datenschutz@dialogschmiede.com</a> möglich. Details zur Verarbeitung Ihrer Daten finden Sie in unserer <a href="datenschutz">Datenschutzerklärung</a>.</p>

            <input type="submit" value="Abschicken" data-wait="Please wait..." class="button form-button w-button"></form>
          <div class="form-success w-form-done">
            <div>Vielen Dank! Wir haben Ihre Nachricht erhalten und melden uns bald.</div>
          </div>
          <div class="form-error w-form-fail">
            <div>Oops! Da ist etwas schiefgelaufen. Bitte versuchen Sie es erneut..</div>
          </div>
        </div>
      </div>
    </div>
    <div class="section grey-section no-top-padding no-bottom-padding">
      <div class="wrapper w-container">
        <div class="footer">
          <div class="footer-about"><a href="/" class="footer-logo w-nav-brand"><img src="images/dialogschmiede_logo_1.png" srcset="images/dialogschmiede_logo_1-p-500.png 500w, images/dialogschmiede_logo_1.png 600w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 42vw, (max-width: 991px) 27vw, 20vw" alt=""></a>
            <p class="paragraph-small">Die Dialogschmiede ist Österreichs <br>führende Dialogagentur.</p>
          </div>
          <div class="footer-nav">
            <h5>Navigation</h5><a href="/" class="footer-link">Home</a><a href="portfolio" class="footer-link">Cases</a><a href="news" class="footer-link">News</a><a href="team" class="footer-link">Team</a><a href="kontakt" class="footer-link w--current">Kontakt</a></div>
          <div class="footer-subscribe">
            <!-- <h5>Jetzt unseren Newsletter abonnieren:</h5>
            <div class="w-form">
              <form id="wf-form-Subscribe-Form" name="wf-form-Subscribe-Form" data-name="Subscribe Form" class="footer-subscribe-form"><input type="email" id="email-4" name="email-4" data-name="Email 4" placeholder="Ihre E-Mail Adresse" maxlength="256" required="" class="input footer-input w-input"><input type="submit" value="Abonnieren" data-wait="Please wait..." class="button w-button"></form>
              <div class="form-success w-form-done">
                <div>Vielen Dank! Wir haben Ihre Nachricht erhalten und melden uns bald.</div>
              </div>
              <div class="form-error w-form-fail">
                <div>Oops! Da ist etwas schiefgelaufen. Bitte versuchen Sie es erneut.</div>
              </div>
            </div>-->
 <?php include "footer.inc.php"; ?>
        </div>
      </div>
    </div>
  </div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>