<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Thu Jan 16 2020 10:06:47 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="5d5fc4f1b6d68d67c002f90f" data-wf-site="5cea4eba7a0da81f0863712a">
<head>
  <meta charset="utf-8">
  <title>Dialogschmiede Cases</title>
  <meta content="Wir bieten Lösungen für Performance-Driven Campaigning: Unsere Erfolgsgeschichten und Kunden im Überblick." name="description">
  <meta content="Dialogschmiede Agentur" property="og:title">
  <meta content="Wir bieten Lösungen für Performance-Driven Campaigning: Unsere Erfolgsgeschichten und Kunden im Überblick." property="og:description">
  <meta content="summary" name="twitter:card">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/dialogschmiede.webflow.css" rel="stylesheet" type="text/css">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]  }});</script> -->
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="images/webclip.png" rel="apple-touch-icon">
<?php include "header.inc.php"; ?></head>
<body class="body">
  <div data-ix="preloader" class="page-loader"></div>
  <div data-ix="page-wrapper-load" class="page-wrapper">
    <div class="contact-popup">
      <div class="popup-wrapper">
        <div class="contact-popup-window w-form">
          <a href="#" data-ix="hide-contact-popup" class="minimize-icon w-inline-block">
            <div class="minimize-icon-line"></div>
          </a>
          <h3>Kontaktieren Sie uns</h3>
          <form id="wf-form-Contact-Popup-Form" name="wf-form-Contact-Popup-Form" data-name="Contact Popup Form" class="contact-window-form"><input type="text" id="Name-3" name="Name" data-name="Name" placeholder="Name" maxlength="256" class="input w-input"><input type="email" id="Email-3" name="Email" data-name="Email" placeholder="E-Mail Adresse" maxlength="256" required="" class="input w-input"><textarea id="Message" name="Message" placeholder="Wie können wir Ihnen helfen?" maxlength="5000" required="" data-name="Message" class="input text-area w-input"></textarea><input type="submit" value="Nachricht senden" data-wait="Please wait..." class="button form-button w-button"></form>
          <div class="form-success window-success w-form-done">
            <div class="text-block-16">Vielen Dank! Wir haben Ihre Nachricht erhalten und melden uns bald.</div>
          </div>
          <div class="form-error window-error w-form-fail">
            <div class="text-block-15">Oops! Da ist etwas schiefgelaufen. Bitte versuchen Sie es erneut.</div>
          </div>
        </div>
        <div data-ix="hide-contact-popup" class="popup-overlay"></div>
      </div>
    </div>
    <div data-collapse="medium" data-animation="default" data-duration="400" data-ix="nav-bar" class="nav-bar w-nav">
      <div class="wrapper w-container"><a href="/" data-ix="logo" class="logo-link w-nav-brand"><img src="images/dialogschmiede_logo_1.png" width="180" srcset="images/dialogschmiede_logo_1-p-500.png 500w, images/dialogschmiede_logo_1.png 600w" sizes="(max-width: 479px) 100vw, 180px" alt=""></a>
        <nav role="navigation" class="nav-menu w-nav-menu"><a href="portfolio" class="nav-link">Cases</a><a href="news" class="nav-link">News</a><a href="team" class="nav-link">Team</a><a href="kontakt" class="nav-link">Kontakt</a></nav>
        <div class="menu-button white-menu-button w-nav-button">
          <div class="w-icon-nav-menu" style="color:black"></div>
        </div>
      </div>
    </div>
    <?php
              include "apicalls.php";
              $itemid = $_GET['itemid'];
              $work = apicall('/collections/5d5fc4f1b6d68d08a702f952/items');
              //  print_r($work);
            //  echo '/collections/5d5fc4f1b6d68d08a702f952/items/'.$itemid;
             foreach($work->items as $id=>$item) {
              $item =  (array)$item;
              
              if($itemid == $item['_id']) {
                $categories = apicall('/collections/5d5fc4f1b6d68d210202f98e/items');
                foreach($categories->items as $c=>$categ) {
                  if($item['work-category'] == $categ->_id) $mycat = $categ->name;
                
               }

              //  echo $item['video']->metadata->html;
                //  print_r($item['video']);
                // https://vimeo.com/361743565/a38f9a216f
                $videourl = substr($item['video']->url,18,9);
                // echo $videourl;

                echo '<div class="section grey-section">
                <div class="wrapper w-container"></div>
              </div>
              <div class="section-header-wrapper">
                <h1 class="heading-6">'.$item['name'].'</h1>
                <p class="section-description">'.$item['short-description'].'</p>
                <div class="divider-2 center"></div>
              </div>
              <div>
                <div class="w-container">
                  <div class="w-video w-embed">
                  <iframe src="https://player.vimeo.com/video/'.$videourl.'?embedparameter=value" width="'.$item['video']->metadata->width.'" height="'.$item['video']->metadata->height.'" frameborder="0" allowfullscreen></iframe>
                </div>
                </div>
              </div>
              <div class="section-header-wrapper">
                <div class="work-details">
                  <div class="work-details-cell">
                    <div class="work-details-cell-header">Kunde:</div>
                    <div class="text-block-13">'.$item['client'].'</div>
                  </div>
                  <div class="work-details-cell">
                    <div class="work-details-cell-header">Kategorie:</div>
                    <div class="text-block-12">'.$mycat.'</div>
                  </div>
                </div>
                <div data-ix="float-behance-card" class="work-presentation w-richtext">
                '.$item['work-presentation'].'</div>
              </div>
              <div>
      <div class="w-container"><img src="'.$item['bild']->url.'" alt="" class="image-10"></div>';
    }
   }   
?>
    </div>
    <div class="section">
      <div class="section-header-wrapper">
        <h2 class="section-header-impressum"><span class="text-span" style="color:black;">Mehr Cases</span></h2>
      </div>
      <div class="wrapper w-container">
        <div class="w-dyn-list">
          <div data-ix="slide-up-1" class="works-v5 w-dyn-items">
          <?php
          foreach($work->items as $id=>$item) {
            $item =  (array)$item;
            // print_r($item['_id']);
            if($itemid != $item['_id']) {
              $categories = apicall('/collections/5d5fc4f1b6d68d210202f98e/items');
              foreach($categories->items as $c=>$categ) {
                if($item['work-category'] == $categ->_id) $mycat = $categ->name;
              
             }

            echo '<div class="work-v5 w-dyn-item">
              <a href="detail_works?itemid='.$item['_id'].'" data-ix="work-card" class="work-card-v5 w-inline-block">
                <div class="work-card-v5-image"><img src="'.$item['preview-image']->url.'" alt="" class="work-card-image">
                  <div class="work-card-info w-hidden-small w-hidden-tiny"><img src="'.$item['client-logo']->url.'" alt="" class="client-logo-image"></div>
                </div>
                <h6 class="work-card-v5-header">'.$item['name'].'</h6>
              </a>
            </div>';
            }
          }
            ?>
          </div>
         
        </div>
      </div>
    </div>
    <div class="section grey-section no-top-padding no-bottom-padding">
      <div class="wrapper w-container">
        <div class="footer">
          <div class="footer-about"><a href="/" class="footer-logo w-nav-brand"><img src="images/dialogschmiede_logo_1.png" srcset="images/dialogschmiede_logo_1-p-500.png 500w, images/dialogschmiede_logo_1.png 600w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 42vw, (max-width: 991px) 27vw, 20vw" alt=""></a>
            <p class="paragraph-small">Die Dialogschmiede ist Österreichs <br>führende Dialogagentur.</p>
          </div>
          <div class="footer-nav">
            <h5>Navigation</h5><a href="/" class="footer-link">Home</a><a href="portfolio" class="footer-link">Cases</a><a href="news" class="footer-link">News</a><a href="team" class="footer-link">Team</a><a href="kontakt" class="footer-link">Kontakt</a></div>
          <div class="footer-subscribe">
            <!--<h5>Jetzt unseren Newsletter abonnieren:</h5>
            <div class="w-form">
              <form id="wf-form-Subscribe-Form" name="wf-form-Subscribe-Form" data-name="Subscribe Form" class="footer-subscribe-form"><input type="email" id="email-4" name="email-4" data-name="Email 4" placeholder="Ihre E-Mail Adresse" maxlength="256" required="" class="input footer-input w-input"><input type="submit" value="Abonnieren" data-wait="Please wait..." class="button w-button"></form>
              <div class="form-success w-form-done">
                <div>Vielen Dank! Wir haben Ihre Nachricht erhalten und melden uns bald.</div>
              </div>
              <div class="form-error w-form-fail">
                <div>Oops! Da ist etwas schiefgelaufen. Bitte versuchen Sie es erneut.</div>
              </div>
            </div>-->
          <?php include "footer.inc.php"; ?></head>
        </div>
      </div>
    </div>
  </div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>