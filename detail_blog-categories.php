<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Thu Jan 16 2020 10:06:47 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="5d5fc4f1b6d68d292602f90b" data-wf-site="5cea4eba7a0da81f0863712a">
<head>
  <meta charset="utf-8">
  <title>Dialogschmiede News</title>
  <meta content="Uno Agency — Webflow CMS Template" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/dialogschmiede.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="images/webclip.png" rel="apple-touch-icon">
<?php include "header.inc.php"; ?></head>
<body class="body">
  <div data-ix="preloader" class="page-loader"></div>
  <div data-ix="page-wrapper-load" class="page-wrapper">
    <div data-collapse="medium" data-animation="default" data-duration="400" data-ix="nav-bar" class="nav-bar w-nav">
      <div class="wrapper w-container"><a href="/" data-ix="logo" class="logo-link w-nav-brand"><img src="images/dialogschmiede_logo_1.png" width="180" srcset="images/dialogschmiede_logo_1-p-500.png 500w, images/dialogschmiede_logo_1.png 600w" sizes="(max-width: 479px) 100vw, 180px" alt=""></a>
        <nav role="navigation" class="nav-menu w-nav-menu"><a href="portfolio" class="nav-link nav-link-white">Cases</a><a href="news" class="nav-link nav-link-white">News</a><a href="team" class="nav-link nav-link-white">Team</a><a href="kontakt" class="nav-link nav-link-white">Kontakt</a></nav>
        <div class="menu-button white-menu-button w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div> <?php
     include "apicalls.php";
    $categories = apicall('/collections/5d5fc4f1b6d68d5ec902f97a/items');
    $catid = $_GET['cat'];
      // echo '<img src="'.$item['preview-image']->url.'">';
      foreach($categories->items as $c=>$categ) {
        if($catid == $categ->_id) $mycat = $categ->name;
      
     }
    ?>
    <div class="page-header-section image-3">
      <div class="page-header-wrapper">
        <h6 class="section-header-impressum"><span class="text-span"><?php echo $mycat; ?></span></h6>
      </div>
    </div>
    <div class="section grey-section">
      <div class="wrapper w-container">
        <div class="w-dyn-list">
          <div data-ix="slide-up-1" class="blog-posts-v1 w-dyn-items">
          <?php 
             
             $news = apicall('/collections/5d5fc4f1b6d68d454902f966/items');
             $categories = apicall('/collections/5d5fc4f1b6d68d5ec902f97a/items');

             foreach($news->items as $id=>$item) {
               $item =  (array)$item;
               // echo '<img src="'.$item['preview-image']->url.'">';
               foreach($categories as $c=>$categ) {
                 if($item['category'] == $categ['_id']) $mycat = $categ['name'];
              }
              if($catid == $item['category']) {
             
                echo '<div class="post-v1 w-dyn-item">
                <a href="detail_blog?itemid='.$item['_id'].'" data-ix="more-link" class="post-card w-inline-block">
                <div class="category post-card-v3-category">'.$mycat.'</div><img src="'.$item['post-thumbnail']->url.'" alt="">
                <div class="post-card-info">
                  <h4 class="post-card-header">'.$item['name'].'</h4>
                  <p class="post-card-description">'.$item['short-description'].'</p>
                  <div class="more-link w-clearfix"><img src="images/more-arrow-icon_DS.png" width="16" alt="" class="more-arrow-icon">
                    <div class="more-link-text"><strong>Mehr Lesen</strong></div>
                    <div class="hover-line"></div>
                  </div>
                </div>
              </a>
            </div>
            ';
            $j++;
              }  
           
          }
          ?>
          </div>
         
        </div>
      </div>
    </div>
    <div class="section grey-section no-top-padding no-bottom-padding">
      <div class="wrapper w-container">
        <div class="footer">
          <div class="footer-about"><a href="/" class="footer-logo w-nav-brand"><img src="images/dialogschmiede_logo_1.png" srcset="images/dialogschmiede_logo_1-p-500.png 500w, images/dialogschmiede_logo_1.png 600w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 42vw, (max-width: 991px) 27vw, 20vw" alt=""></a>
            <p class="paragraph-small">Die Dialogschmiede ist Österreichs <br>führende Dialogagentur.</p>
          </div>
          <div class="footer-nav">
            <h5>Navigation</h5><a href="/" class="footer-link">Home</a><a href="portfolio" class="footer-link">Cases</a><a href="news" class="footer-link">News</a><a href="team" class="footer-link">Team</a><a href="kontakt" class="footer-link">Kontakt</a></div>
          <div class="footer-subscribe">
            <!--<h5>Jetzt unseren Newsletter abonnieren:</h5>
            <div class="w-form">
              <form id="wf-form-Subscribe-Form" name="wf-form-Subscribe-Form" data-name="Subscribe Form" class="footer-subscribe-form"><input type="email" id="email-4" name="email-4" data-name="Email 4" placeholder="Ihre E-Mail Adresse" maxlength="256" required="" class="input footer-input w-input"><input type="submit" value="Abonnieren" data-wait="Please wait..." class="button w-button"></form>
              <div class="form-success w-form-done">
                <div>Vielen Dank! Wir haben Ihre Nachricht erhalten und melden uns bald.</div>
              </div>
              <div class="form-error w-form-fail">
                <div>Oops! Da ist etwas schiefgelaufen. Bitte versuchen Sie es erneut.</div>
              </div>
            </div>-->
 <?php include "footer.inc.php"; ?></head>
        </div>
      </div>
    </div>
  </div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>