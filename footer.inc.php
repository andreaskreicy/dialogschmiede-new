          <div class="social-icons footer-v2-icons">
            <a href="https://www.instagram.com/dialogschmiede/?hl=de" data-ix="social-icon" target="_blank" class="social-icon-container w-inline-block"><img src="images/instagram-icon.svg" alt="" class="social-icon"><img src="images/instagram-icon-color.svg" alt="" class="social-icon-hover"></a>
            <a href="https://www.facebook.com/Dialogschmiede/" data-ix="social-icon" target="_blank" class="social-icon-container w-inline-block"><img src="images/facebook-icon.svg" alt="" class="social-icon"><img src="images/facebook-icon-color.svg" alt="" class="social-icon-hover"></a>
            <a href="https://www.linkedin.com/company/dialogschmiede-gmbh/" data-ix="social-icon" target="_blank" class="social-icon-container w-inline-block"><img src="images/linkedin_black.svg" style="height:20px; " alt="" class="social-icon"><img  style="height:20px; " src="images/linkedin_blue.svg" alt="" class="social-icon-hover"></a>
            <a href="https://www.xing.com/companies/dialogschmiede" data-ix="social-icon" target="_blank" class="social-icon-container w-inline-block"><img src="images/xing_black.svg" style="height:18px; " alt="" class="social-icon"><img  style="height:18px; " src="images/xing_green.svg" alt="" class="social-icon-hover"></a>
          
            <div class="certificates" style="float:left; margin-left:30px; margin-top:-30px;">
              <img src="images/tag_partner.png" width="150"/>
            </div>
            <div class="certificates" style="float:left; margin-left:30px; margin-top:-30px;">
              <img src="images/trust_partner.png" width="150"/>
            </div>
          </div>
          
          </div>
          <div class="footer-bottom">
            <div class="footer-bottom-text-left">
              <div class="footer-bottom-text-left">© 2021 Dialogschmiede GmbH, All rights reserved.</div>
            </div>
            <div><a href="impressum" class="footer-bottom-link-right">Impressum</a><a href="agb" class="footer-bottom-link-right">AGB</a><a href="agb-ch" class="footer-bottom-link-right">AGB Schweiz</a><a href="agb-de" class="footer-bottom-link-right">AGB Deutschland</a><a href="datenschutz" class="footer-bottom-link-right">Datenschutz</a></div>
          </div>