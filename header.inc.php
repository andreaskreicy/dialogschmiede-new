
<script type="application/javascript" src="https://app.usercentrics.eu/latest/main.js" id="E_ZVlriR"></script>
<script type="application/javascript" src="https://privacy-proxy.usercentrics.eu/latest/uc-block.bundle.js"></script>
<meta data-privacy-proxy-server="https://privacy-proxy-server.usercentrics.eu">

<!-- <script type="text/plain" data-usercentrics="Google Analytics">var gaProperty = 'UA-9060529-1';

var disableStr = 'ga-disable-' + gaProperty;

if (document.cookie.indexOf(disableStr + '=true') > -1) {

    window[disableStr] = true;

}

function gaOptout() {

    document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';

    window[disableStr] = true;

    alert('Das Tracking durch Google Analytics wurde in Ihrem Browser für diese Website deaktiviert.');

}

</script>

<script type="text/plain" async src="https://www.googletagmanager.com/gtag/js?id=UA-9060529-1" data-usercentrics="Google Analytics"></script>

<script type="text/plain" data-usercentrics="Google Analytics">

    window.dataLayer = window.dataLayer || [];

    function gtag(){dataLayer.push(arguments);}

    gtag('js', new Date());

    gtag('config', 'UA-9060529-1', { 'anonymize_ip': true });

</script> -->


<!-- Matomo -->
<script type="text/plain" data-usercentrics="Matomo">
  var _paq = window._paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
  _paq.push(["setCookieDomain", "*.dialogschmiede.com"]);
  _paq.push(["setDomains", ["*.dialogschmiede.com"]]);
  _paq.push(["setDoNotTrack", true]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="https://dialogschmiede.matomo.cloud/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src='//cdn.matomo.cloud/dialogschmiede.matomo.cloud/matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="https://dialogschmiede.matomo.cloud/matomo.php?idsite=1&amp;rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End Matomo Code -->



<script type="text/plain"  data-usercentrics="Droid" src="https://s3.eu-central-1.amazonaws.com/trace-fra.droidmarketing.com/dialogschmiede_website.min.js"></script>
<!-- Campaign Web Tracking Code -->


<!-- Please insert the following code between your HTML document head tags to maintain a common reference to a unique visitor across one or more external web tracked sites. -->
<!-- <meta name="com.silverpop.brandeddomains" content="www.pages06.net,behavioral-bank-austria.digitalschmiede.at,behavioral-showcase.digitalschmiede.at,denzel.lokal,dialogschmiede.at,dialogschmiede.com,digitalschmiede.com,digitalschmiede.mkt5616.com,droidmarketing.com,ibeacon.digitalschmiede.at,oc1000.digitalschmiede.com,serfaus-fiss-ladis.at,www.dialogschmiede.com" /> -->


<!-- Optionally uncomment the following code between your HTML document head tags if you use Campaign Conversion Tracking (COT). -->
<!--<meta name="com.silverpop.cothost" content="pod6.ibmmarketingcloud.com" />-->


<!-- <script src="http://contentz.mkt61.net/lp/static/js/iMAWebCookie.js?3e5c6430-14672191d7e-29238c14b213658164f6bb2ee4e49697&h=www.pages06.net" type="text/javascript"></script> -->


<!-- For external web sites running under SSL protocol (https://) please uncomment this block and comment out the non-secure (http://) implementation. -->
<!-- <script src="https://www.sc.pages06.net/lp/static/js/iMAWebCookie.js?3e5c6430-14672191d7e-29238c14b213658164f6bb2ee4e49697&h=www.pages06.net" type="text/javascript"></script> -->


