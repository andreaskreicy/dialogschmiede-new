<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Thu Jan 16 2020 10:06:47 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="5d5fc4f1b6d68d29a202fa99" data-wf-site="5cea4eba7a0da81f0863712a">
<head>
  <meta charset="utf-8">
  <title>Dialogschmiede Team</title>
  <meta content="Das Dialogschmiede Team. Mit großer Leidenschaft wird hier an neuartigen Kommunikationsformen zwischen Marke und Mensch getüftelt." name="description">
  <meta content="Dialogschmiede Agentur" property="og:title">
  <meta content="Das Dialogschmiede Team. Mit großer Leidenschaft wird hier an neuartigen Kommunikationsformen zwischen Marke und Mensch getüftelt." property="og:description">
   <meta content="summary" name="twitter:card">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/dialogschmiede.webflow.css" rel="stylesheet" type="text/css">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]  }});</script> -->
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="images/webclip.png" rel="apple-touch-icon">
<?php include "header.inc.php"; ?></head>
<body class="body">
  <div data-ix="preloader" class="page-loader"></div>
  <div data-ix="page-wrapper-load" class="page-wrapper">
    <div class="contact-popup">
      <div class="popup-wrapper">
        <div class="contact-popup-window w-form">
          <a href="#" data-ix="hide-contact-popup" class="minimize-icon w-inline-block">
            <div class="minimize-icon-line"></div>
          </a>
          <h3>Kontaktieren Sie uns</h3>
          <form id="wf-form-Contact-Popup-Form" name="wf-form-Contact-Popup-Form" data-name="Contact Popup Form" class="contact-window-form"><input type="text" id="Name-3" name="Name" data-name="Name" placeholder="Name" maxlength="256" class="input w-input"><input type="email" id="Email-3" name="Email" data-name="Email" placeholder="E-Mail Adresse" maxlength="256" required="" class="input w-input"><textarea id="Message" name="Message" placeholder="Wie können wir Ihnen helfen?" maxlength="5000" required="" data-name="Message" class="input text-area w-input"></textarea><input type="submit" value="Nachricht senden" data-wait="Please wait..." class="button form-button w-button"></form>
          <div class="form-success window-success w-form-done">
            <div class="text-block-16">Vielen Dank! Wir haben Ihre Nachricht erhalten und melden uns bald.</div>
          </div>
          <div class="form-error window-error w-form-fail">
            <div class="text-block-15">Oops! Da ist etwas schiefgelaufen. Bitte versuchen Sie es erneut.</div>
          </div>
        </div>
        <div data-ix="hide-contact-popup" class="popup-overlay"></div>
      </div>
    </div>
    <div data-collapse="medium" data-animation="default" data-duration="400" data-ix="nav-bar" class="nav-bar w-nav">
      <div class="wrapper w-container"><a href="/" data-ix="logo" class="logo-link w-nav-brand"><img src="images/dialogschmiede_logo_1.png" width="180" srcset="images/dialogschmiede_logo_1-p-500.png 500w, images/dialogschmiede_logo_1.png 600w" sizes="(max-width: 479px) 100vw, 180px" alt=""></a>
        <nav role="navigation" class="nav-menu w-nav-menu"><a href="portfolio" class="nav-link nav-link-white">Cases</a><a href="news" class="nav-link nav-link-white">News</a><a href="team" class="nav-link nav-link-white w--current">Team</a><a href="kontakt" class="nav-link nav-link-white">Kontakt</a></nav>
        <div class="menu-button white-menu-button w-nav-button">
          <div class="w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div class="page-header-section about" alt="In die Tastatur tippend." title="In die Tastatur tippend.">
      <div class="page-header-wrapper">
        <h1 class="page-header"><strong>One Passion<br>One Team</strong></h1>
      </div>
    </div>
    <div class="section">
      <div class="section-header-wrapper">
        <h2 class="section-header-impressum"><strong>Ein Blick hinter die Daten</strong></h2>
        <div class="divider center"></div>
        <p class="grey-text">Laut, bunt und alles andere als langweilig, liebt die Dialogschmiede vor allem eines: Reden. Nie einer Antwort verlegen, meidet die Dialogschmiede geflissentlich jede Form von Monolog. Denn lesen, klicken, versenden, zuhören, verstehen und auch tüfteln lässt es sich gemeinsam einfach besser.</p>
      </div>
      <div class="wrapper w-dyn-list">
        <div class="collection-list-2 w-dyn-items">
          <?php
           include "apicalls.php";
           $members = apicall('/collections/5d5fc4f1b6d68d31b102f9a2/items');
           krsort($members->items);
           $count = $members->count;
        
         for($i=0;  $i <=40; $i++ ) {
          foreach($members->items as $id=>$member) {
            $member =  (array)$member;
           
            if($member['order'] == $i) {
              echo '
              <div class="collection-item w-dyn-item">
                <div data-w-id="08633866-c57f-21e5-73eb-78836f6edaf9" class="team-member-photo-wrapper">
                  <div data-w-id="8ca954e1-a684-f3c0-0a7d-bc07916822fa" style="opacity:0" class="team-bio-wrapper">
                    <p class="text-team-bio">'.$member['biografie-individuell'].'</p>
                  </div><img src="'.$member['profile-picture']->url.'" alt="" sizes="(max-width: 479px) 100vw, (max-width: 767px) 47vw, (max-width: 991px) 23vw, 22vw" 
                  srcset="'.$member['profile-picture']->url.' 500w, 
                  '.$member['profile-picture']->url.' 800w, 
                  '.$member['profile-picture']->url.' 904w" class="team-member-photo">
                  <div class="category team-member-role">'.$member['job-title'].'</div>
                </div>
                <h6>'.$member['name'].'</h6>
              </div>
          ';
            }
            
           }
        }
           
        ?>
       </div> 
      
    </div>
    <div id="Scroll-Section" class="section section-job-application">
      <div class="wrapper w-container">
        <div class="side-frame reversed">
          <div class="side-media">
            <div class="frame"><img src="images/website_team.png" srcset="images/website_team.png 500w, images/website_team.png 800w, images/website_team.png 1080w, images/website_team.png 1200w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 43vw, (max-width: 991px) 44vw, 34vw" alt="Entspannung am Arbeitsplatz"  title="Entspannung am Arbeitsplatz" class="frame-image"></div>
          </div>
          <div data-ix="slide-right" class="side-content-small">
            <h2 class="section-header-impressum" style="margin:0; padding:0;"><strong>Interessiert<br>an der Zukunft des Marketings?</strong><br></h2>
            <br>
            Dann bewirb dich gleich auf unsere aktuellen Jobs oder gerne auch initiativ an <a href="mailto:jobs@dialogschmiede.com">jobs@dialogschmiede.com</a>! <br> Unsere aktuelle Jobausschreibung findest du <a href="https://www.karriere.at/jobs/dialogschmiede" target="_blank">hier</a>.<br>Wir freuen uns darauf dich kennenzulernen.
         </div>
        </div><br><br>
        <!-- <div id="psJobWidget"></div> -->
        <!-- <script src="https://datacentric-network.jobbase.io/widget/iframe.js?config=dbl6ftf0"></script> -->

        <!-- </div> -->
    </div>
    <div class="section grey-section no-top-padding no-bottom-padding">
      <div class="wrapper w-container">
        <div class="footer">
          <div class="footer-about"><a href="/" class="footer-logo w-nav-brand"><img src="images/dialogschmiede_logo_1.png" srcset="images/dialogschmiede_logo_1-p-500.png 500w, images/dialogschmiede_logo_1.png 600w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 42vw, (max-width: 991px) 27vw, 20vw" alt=""></a>
            <p class="paragraph-small">Die Dialogschmiede ist Österreichs <br>führende Dialogagentur.</p>
          </div>
          <div class="footer-nav">
            <h5>Navigation</h5><a href="/" class="footer-link">Home</a><a href="portfolio" class="footer-link">Cases</a><a href="news" class="footer-link">News</a><a href="team" class="footer-link w--current">Team</a><a href="kontakt" class="footer-link">Kontakt</a></div>
          <div class="footer-subscribe">
            <!--<h5>Jetzt unseren Newsletter abonnieren:</h5>
            <div class="w-form">
              <form id="wf-form-Subscribe-Form" name="wf-form-Subscribe-Form" data-name="Subscribe Form" class="footer-subscribe-form"><input type="email" id="email-4" name="email-4" data-name="Email 4" placeholder="Ihre E-Mail Adresse" maxlength="256" required="" class="input footer-input w-input"><input type="submit" value="Abonnieren" data-wait="Please wait..." class="button w-button"></form>
              <div class="form-success w-form-done">
                <div>Vielen Dank! Wir haben Ihre Nachricht erhalten und melden uns bald.</div>
              </div>
              <div class="form-error w-form-fail">
                <div>Oops! Da ist etwas schiefgelaufen. Bitte versuchen Sie es erneut.</div>
              </div>
            </div>-->
 <?php include "footer.inc.php"; ?>
        </div>
      </div>
    </div>
  </div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>