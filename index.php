<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Thu Jan 16 2020 10:06:47 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="5d5fc4f1b6d68d01e502f903" data-wf-site="5cea4eba7a0da81f0863712a">
<head>
  <meta charset="utf-8">
  <title>Dialogschmiede – Österreichs führende Dialogagentur</title>
  <meta content="Die Dialogschmiede ist Österreichs führende Dialogagentur. Wir helfen Ihnen mit verhaltensbasierter Kommunikation und Marketing Automation." name="description">
  <meta content="Dialogschmiede Agentur" property="og:title">
  <meta content="Die Dialogschmiede ist Österreichs führende Dialogagentur. Wir helfen Ihnen mit verhaltensbasierter Kommunikation und Marketing Automation." property="og:description">
  <meta content="summary" name="twitter:card">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/dialogschmiede.webflow.css" rel="stylesheet" type="text/css">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]  }});</script> -->
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="images/webclip.png" rel="apple-touch-icon">
  
<?php include "header.inc.php"; ?></head>
<body class="body">
  <div data-ix="preloader" class="page-loader"></div>
  <div data-ix="page-wrapper-load" class="page-wrapper">
    
    <div class="contact-popup">
      <div class="popup-wrapper">
        <div class="contact-popup-window w-form">
          <a href="#" data-ix="hide-contact-popup" class="minimize-icon w-inline-block">
            <div class="minimize-icon-line"></div>
          </a>
          <h6>Hier können Sie uns eine unverbindliche Nachricht hinterlassen. Wir freuen uns über Ihr Interesse und melden uns sobald wie möglich zurück!</h6>
          <form  id="wf-form-Contact-Popup-Form-modal" name="wf-form-Contact-Popup-Form" data-name="Contact Popup Form" class="contact-window-form">
          <input type="text" id="Name-3" name="Name" data-name="Name" placeholder="Name" maxlength="256" class="input w-input">
          <input type="email" id="Email-3" name="Email" data-name="Email" placeholder="E-Mail Adresse" maxlength="256" required="" class="input w-input">
          <textarea id="Message" name="Message" placeholder="Wie können wir Ihnen helfen?" maxlength="5000" required="" data-name="Message" class="input text-area w-input"></textarea>
          <br> <p class="grey-text" style="font-size:13px;">Durch das Abschicken der Nachricht, stimmen Sie zu, dass Ihre Angaben und Daten zur Beantwortung Ihrer Anfrage elektronisch erhoben und gespeichert werden. Ein Widerruf ist jederzeit per Mail an <a href="mailto:datenschutz@dialogschmiede.com">datenschutz@dialogschmiede.com</a> möglich. Details zur Verarbeitung Ihrer Daten finden Sie in unserer <a href="datenschutz">Datenschutzerklärung</a>.</p>
          <br><input type="submit" value="Nachricht senden" data-wait="Please wait..." class="button form-button w-button"></form>
          <div class="form-success window-success w-form-done">
            <div class="text-block-16">Vielen Dank! Wir haben Ihre Nachricht erhalten und melden uns bald.</div>
            
          </div>
          <div class="form-error window-error w-form-fail">
            <div class="text-block-15">Oops! Da ist etwas schiefgelaufen. Bitte versuchen Sie es erneut.</div>
          </div>
        </div>
        <div data-ix="hide-contact-popup" class="popup-overlay"></div>
      </div>
    </div>
    <div alt="Artdirektorin arbeitet am Tablet." data-poster-url="videos/Video_Startseite_v2-poster-00001.jpg" data-video-urls="videos/StartseiteVideo_Nov2020_kleiner.mp4,videos/StartseiteVideo_Nov2020_kleiner.webm" data-autoplay="true" data-loop="true" data-wf-ignore="true" class="hero-section-video w-background-video w-background-video-atom"><video autoplay="" loop="" style="background-image:url(&quot;videos/Video_Startseite_v2-poster-00001.jpg&quot;)" muted="" playsinline="" data-wf-ignore="true" data-object-fit="cover"><source src="videos/StartseiteVideo_Nov2020_kleiner.mp4" data-wf-ignore="true"><source src="videos/StartseiteVideo_Nov2020_kleiner.webm" data-wf-ignore="true"></video>
      <div data-collapse="medium" data-animation="default" data-duration="400" data-ix="nav-bar" class="nav-bar w-nav">
        <div class="wrapper w-container"><a href="/" data-ix="logo" class="logo-link w-nav-brand w--current"><img src="images/dialogschmiede_logo_1.png" width="180" srcset="images/dialogschmiede_logo_1-p-500.png 500w, images/dialogschmiede_logo_1.png 600w" sizes="(max-width: 479px) 100vw, 180px" alt=""></a>
          <nav role="navigation" class="nav-menu w-nav-menu"><a href="portfolio" class="nav-link nav-link-white">Cases</a><a href="news" class="nav-link nav-link-white">News</a><a href="team" class="nav-link nav-link-white">Team</a><a href="kontakt" class="nav-link nav-link-white">Kontakt</a></nav>
          <div class="menu-button white-menu-button w-nav-button">
            <div class="w-icon-nav-menu"></div>
          </div>
        </div>
      </div>
      <div data-ix="slide-up-2" class="hero-light-image-content">
        <h1 class="hero-light-image-header"><span class="text-span">Interessiert an der Zukunft des Marketings? </span></h1>
        <p class="hero-split-text">Willkommen in der DIALOGSCHMIEDE!</p><a href="https://www.karriere.at/jobs/dialogschmiede" target="_blank" class="button w-button">Aktuelle Jobs</a></div>
    </div>
    <div class="section grey-section">
      <div class="section-header-wrapper">
      <!-- <p class="grey-text">Relevanz entsteht durch die Synchronisation von Menschen und Marken. Mit unseren Omnichannel-Kundendialog Lösungen steigern wir die Zufriedenheit Ihrer Kunden und gleichzeitig Ihren Sales-Anteil.</p> -->
        <p class="grey-text">Wir beobachten, sammeln Daten, konzipieren verhaltensbasierte Kampagnen, automatisieren und optimieren relevante Kommunikation auf allen digitalen Touchpoints. Mit unserer Hilfe wird Kundendialog empathisch und messbar.</p>
      </div>  
      <div class="section-header-wrapper" style="max-width:900px;">
          <h2 class="section-header-impressum"><strong id="Marketingautomation" class="bold-text-3">Marketing Automation leicht erkl&auml;rt</strong></h2>
        <div class="divider center"></div>
      </div>
      <div style="margin-bottom:80px;">
          <div>
            <video id="vid1" loop=""  playsinline=""  onClick="aud_play_pause()" style="width:60%;">
            <source src="videos/DS_Einreichvideo_FINAL_1_2.mp4">
            <source src="videos/DS_Einreichvideo_FINAL_1_2.webm">
            </video>
         </div>
      
      </div>

      <div class="section-header-wrapper">
          <h2 class="section-header-impressum"><strong id="Leistungsspektrum" class="bold-text-4">Leistungsspektrum</strong></h2>
        <div class="divider center"></div>
      </div>
      <div class="wrapper w-container">
        <div class="feature-cells">
          <div class="feature-cell"><img src="images/kreation_128x128.png" width="64" alt="" class="feature-icon">
            <h5><strong>Innovation &amp; Kreation</strong></h5>
            <p class="paragraph-small">Wir helfen Ihnen, neue und innovative Wege zu finden, um mit Ihren Kunden zu interagieren. Mit automatisierten, verhaltensbasierten Kampagnen.</p>
          </div>
          <div class="feature-cell"><img src="images/itsupport_128x128.png" width="64" alt="" class="feature-icon">
            <h5><strong>IT Support &amp; Systembetrieb</strong></h5>
            <p class="paragraph-small">Wir helfen Ihnen, mit unseren Ressourcen digitale Touchpoints zu entwickeln. Dabei betreuen und servicieren wir die gesamte Marketing Automation-Landschaft, deren Datenbanken und DMPs.</p>
          </div>
          <div class="feature-cell"><img src="images/CJM.png" width="64" alt="" class="feature-icon">
            <h5>Customer Journey Management</h5>
            <p class="paragraph-small">Wir helfen Ihnen, aus Dialog Mehrwert zu schaffen. Inhalte werden zu dem Zeitpunkt und über den Kanal kommuniziert, der für Ihre Kunden relevant ist.</p>
          </div>
          <div class="feature-cell"><img src="images/Datenanalysen_128x128.png" width="64" alt="" class="feature-icon">
            <h5><strong>Datenanalysen &amp; Insights</strong></h5>
            <p class="paragraph-small">Wir helfen Ihnen, Zielgruppen besser zu verstehen. Auf Basis von Verhaltensdaten bauen wir Personas, Segmentierungen und Insight-Analysen. Kampagnen werden in Echtzeit über Dashboards gesteuert und durch ständiges lernen optimiert. </p>
          </div>
          <div class="feature-cell"><img src="images/marketingautomation.png" width="64" alt="" class="feature-icon">
            <h5><strong>Marketing Automation</strong></h5>
            <p class="paragraph-small">Wir helfen Ihnen, Marketing Automation und data-driven Campaigning zu betreiben: Gemeinsam mit unseren Technologiepartnern IBM Acoustics, MAPP, Droid Marketing und Salesforce.</p>
          </div>
          <div class="feature-cell"><img src="images/produktionwerbemittel.png" width="64" alt="" class="feature-icon">
            <h5><strong>Kreation Werbemittel</strong></h5>
            <p class="paragraph-small">Wir helfen Ihnen, Werbung in kürzester Durchlaufzeit effizient umzusetzen. Eine eigene Abteilung gestaltet dynamische, innovative und technisch einwandfreie Werbemittel.</p>
          </div>
        </div>
      </div>
      <div class="section-header-wrapper section-header-secondary">
        <h2 class="section-header-impressum"><strong class="bold-text-5">Damit schaffen wir:<br></strong></h2>
        <div class="divider center"></div>
      </div>
      <div class="wrapper w-container">
        <div class="feature-cells">
          <!-- <div class="feature-cell feature-cell-grey"><img src="images/icon_effizienz.png" width="64" alt="" class="feature-icon">
            <h5><strong>Effizienz</strong></h5>
            <p class="paragraph-small">Die Optimierung jedes digitalen Touchpoints und deren Messung in Echtzeit.</p>
          </div> -->
          <div class="feature-cell"><img src="images/Icon_kundennaehe.png" width="64" alt="" class="feature-icon">
            <h5><strong>Kundennähe</strong></h5>
            <p class="paragraph-small">Einen automatisierten, laufenden Dialog mit dem Konsumenten. Relevanz stärkt die Kundennähe und zugleich Ihre Marke.</p>
          </div>
          <div class="feature-cell"><img src="images/Icon_verkaufen.png" width="64" alt="" class="feature-icon">
            <h5><strong>Verkauf</strong></h5>
            <p class="paragraph-small">Steigende Online- und Offlineumsätze und mehr Interaktion mit den Kunden. Ihre Verkaufszahlen sind unser härtester KPI.</p>
          </div>
          <div class="feature-cell"><img src="images/icon_businessintel.png" width="64" alt="" class="feature-icon">
            <h5><strong>Business Intelligence &amp; Insights</strong></h5>
            <p class="paragraph-small">Einblicke in konkrete Konsumenten-Motive. Die Analysen helfen Ihnen bei zukünftigen Marketing-Entscheidungen.</p>
          </div>
        </div>
      </div>
    </div>
    
    <div class="section yellow-section">
      <h2 class="white-text quote-text">So entsteht mehr Relevanz in der<br>Kommunikation und im Dialog.</h2>
    </div>
    <div class="section-2">
      <div class="wrapper w-container">
        <div class="section-header-wrapper">
          <h2 class="section-header-impressum">Cases</h2>
          <div class="divider center"></div>
          <p class="grey-text">Mit always-on Kampagnen etablieren wir treue Kundenkreise, stärken Markenwerte und erhöhen den Income pro Kunde. Marke endet für uns nicht beim Kauf, sondern in der laufenden Servicierung der Konsumenten. </p>
          <div class="vertical-header"></div>
        </div>
        <div class="collection-list-wrapper w-dyn-list">
          <div data-ix="slide-up-1" class="works-v4 w-dyn-items">
          <?php 
              include "apicalls.php";
              $work = apicall('/collections/5d5fc4f1b6d68d08a702f952/items');
                // print_r($work);
              foreach($work->items as $id=>$item) {
                $item =  (array)$item;
                if($id <4) {
                  if($item["published-on"]!='') {
                    // echo '<img src="'.$item['preview-image']->url.'">';
                    echo '<div class="work-v4 w-dyn-item">
                    <a href="detail_works?itemid='.$item['_id'].'" data-ix="work-card" class="work-card-v4 w-inline-block">
                    <img src="'.$item['preview-image']->url.'" alt="" class="work-card-image">
                    <div class="work-card-info">
                    <img src="'.$item['client-logo']->url.'" alt="" class="client-logo-image"></div>
                    </a>
                    </div>';
                  }
                }
                
              }
              ?>
          </div>
         
        </div>
        <div class="center buttonmehrkunden"><a href="portfolio" class="button w-button">Mehr Projekte</a></div>
      </div><a data-ix="show-contact-popup" href="#" class="contact-float-button w-inline-block"><img src="images/mail-icon.svg" alt=""></a></div>
    <div id="Contact" class="section side-content-section">
      <div class="side-image-wrapper padding-right">
        <div class="side-image image-3" title="Motivierte Person vor Laptop." alt="Motivierte Person vor Laptop."></div>
      </div>
      <div class="side-content-wrapper right">
        <div data-ix="slide-right" class="side-content-big">
          <h2 class="section-header-impressum"><strong>Droid Creator</strong></h2>
          <div class="divider"></div>
          <p class="section-description grey-text">Ob für Newsletter, Banner oder Website-Personalisierung: Der Droid Creator mit integriertem Briefing-Tool bietet für Ihre Werbemittel dynamische und effiziente Lösungen.</p>
          <a class="button w-button" target="_blank" href="https://cloud.droidmarketing.com/de/creator">Details zum High-End-Editor</a>
        </div>
      </div>
    </div>
    <div id="Contact" class="section side-content-section mobile-reversed">
      <div class="side-content-wrapper left">
        <div data-ix="slide-left" class="side-content-big">
          <h2 class="section-header-impressum">Droid Marketing Cloud</h2>
          <div class="divider"></div>
          <p class="section-description grey-text">Smarte Datenverwaltung ist die Basis für relevante Kommunikation auf allen Kanälen. Datensilos aus CRM, Kaufdaten und Kampagnendaten werden aufgelöst und gezielt eingesetzt.</p>
          <a class="button w-button" target="_blank" href="https://cloud.droidmarketing.com/de">Wie Droid Daten verwandelt</a>
        </div>
      </div>
      <div class="side-image-wrapper padding-left">
        <div class="side-image image-4" title="Lachende KollegInnen" alt="Lachende KollegInnen"></div>
      </div>
    </div>
    <div id="Contact" class="section side-content-section">
      <div class="side-image-wrapper padding-right">
        <div class="side-image image-1" alt="Droid-Website am Notebook." title="Droid-Website am Notebook."></div>
      </div>
      <div class="side-content-wrapper right">
        <div data-ix="slide-right" class="side-content-big">
          <h2 class="section-header-impressum"><strong class="bold-text-6">Droid Push-Hub</strong></h2>
          <div class="divider"></div>
          <p class="section-description grey-text">Datenbanken mit unterschiedlichen Formatierungen werden in ein einheitliches System übersetzt. Droid Push-Hub transkribiert Daten in beide Richtungen in eine lesbare Form.</p>
        </div>
      </div>
    </div>
    
    
    <div class="section facts-v1-section" style="margin-top:140px;" alt="In die Tastatur tippend." title="In die Tastatur tippend.">
      <div class="section-header-wrapper section-header-secondary">
        <div class="text-block-8"><strong class="whitehl">Zahlen &amp; Fakten</strong></div>
        <div class="divider center"></div>
      </div>
      <div class="wrapper">
        <div class="facts dark-bg-facts">
          <div data-ix="slide-up-1" class="fact-left"><img src="images/Icons_128x128_3_v2.png" alt="" class="fact-icon">
            <div class="fact-info">
              <div class="fact-number"><span>Top</span></div>
              <h5 class="fact-header">Dialog / CRM - Agentur</h5>
            </div>
          </div>
          <div data-ix="slide-up-2" class="fact-left"><img src="images/icon_128x128_mitarbeiter.png" alt="" class="fact-icon">
            <div class="fact-info">
              <div class="fact-number">25+</div>
              <h5 class="fact-header">Mitarbeiter</h5>
            </div>
          </div>
          <div data-ix="slide-up-3" class="fact-left"><img src="images/kaffetassen-icon.png" alt="" class="fact-icon">
            <div class="fact-info">
              <div class="fact-number">100+</div>
              <h5 class="fact-header">Kaffeetassen täglich</h5>
            </div>
          </div>
          <div data-ix="slide-up-4" class="fact-left"><img src="images/Icons_128x128_1_v2.png" alt="" class="fact-icon">
            <div class="fact-info">
              <div class="fact-number">134+</div>
              <h5 class="fact-header">Awards</h5>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="Scroll-Section" class="section grey-section">
      <div class="wrapper w-container">
        <div class="side-frame reversed">
          <div data-ix="slide-right" class="side-content-small">
            <h2 class="section-header-impressum"><strong class="bold-text">DATACENTRIC.NETWORK</strong><br></h2>
            <div class="divider"></div>
            <p class="section-description grey-text">Um alle Bereiche des automatisierten Kundendialogs abzudecken, haben wir das Netzwerk DATACENTRIC gegründet. Der Fokus liegt auf DSGVO, Data-Analytics, Media-Lösungen und kreative Media-Assets. Unsere Experten können Sie damit in allen Belangen beraten und weiterhelfen.<br></p>
            <a class="button w-button" target="_blank" href="https://datacentric.network/">Vision & Mission</a>
          </div>
          <div class="side-media">
            <div class="frame"><img src="images/Headerbild_probe_v9.jpg" alt="" class="frame-image"></div>
          </div>
        </div>
      </div>
    </div>

    <div class="section">
      
        <h2>Auszug unserer Kunden</h2>
        <div class="divider center"></div>
      
        <div class="wrapper">
      <div class="facts">
        <div  data-ix="slide-up-1" class="fact-left"><img src="images/01_Magenta.png" alt="" class="fact-icon-client">
        </div>
        <div data-ix="slide-up-2" class="fact-left"><img src="images/02_Bipa.png" alt="" class="fact-icon-client">
        </div>
        <div data-ix="slide-up-3" class="fact-left"><img src="images/aryzta.png" alt="" class="fact-icon-client">
        </div>
        
        <div data-ix="slide-up-4" class="fact-left"><img src="images/09_Ernstings.png" alt="" class="fact-icon-client">
        </div>
       
        <div data-ix="slide-up-5" class="fact-left"><img src="images/zellamsee.png" alt="" class="fact-icon-client">
        </div>
      </div>
      <div class="facts">
        
        <div  data-ix="slide-up-1" class="fact-left"><img src="images/donau.png" alt="" class="fact-icon-client">
        </div>
        <div data-ix="slide-up-2" class="fact-left"><img src="images/dinersclub.png" alt="" class="fact-icon-client">
        </div>
        
        <div data-ix="slide-up-2" class="fact-left"><img src="images/besins.png" alt="" class="fact-icon-client">
        </div>
        <div data-ix="slide-up-4" class="fact-left"><img src="images/rotax.png" alt="" class="fact-icon-client">
        </div>
        <div data-ix="slide-up-5" class="fact-left"><img src="images/10_KSV.png" alt="" class="fact-icon-client">
        </div>
      </div>
      <div class="facts">
        <div  class="fact-left">
          </div>
          <div  class="fact-left"><img src="images/ghg.png" alt="" class="fact-icon-client">
        </div>
        <div data-ix="slide-up-2" class="fact-left"><img src="images/12_serfaus.png" alt="" class="fact-icon-client">
        </div>
        <div data-ix="slide-up-3" class="fact-left"><img src="images/13_bettenreiter.png" alt="" class="fact-icon-client">
        </div>
        <div data-ix="slide-up-3" class="fact-left"><img src="images/08_WienerStaedtische.png" alt="" class="fact-icon-client">
        </div>
        <div  class="fact-left">
          </div>
      </div>
    </div>
    </div>


    <div class="section grey-section">
      <div data-ix="slide-left" class="section-header-wrapper">
        <h2 class="section-header-impressum">Zeit zum Kennenlernen<br></h2>
        <div class="divider center"></div>
        <p class="grey-text">Wie können wir Ihnen weiterhelfen? <br>Wir sind Berater, Kreative, Manager und Begleiter in allen Datenphasen. Erfahren Sie mehr über Ihre Möglichkeiten des automatisierten Marketings. Einfach und unverbindlich. </p>
      </div>
      <div class="wrapper wrapper-contact w-container">
        <div class="side-feature-media">
          <div class="contact-card">
            <div class="contact-card-info">
              <h5 class="heading-2">Wien</h5>
              <div class="contact-card-line"><img src="images/pin-icon_1pin-icon.png" width="24" alt="">
                <div class="contact-card-line-text">Ungargasse 64-66/1/110<br>1030 Wien<br>Österreich</div>
              </div>
              <div class="contact-card-line"><img src="images/phone-icon_1phone-icon.png" width="24" alt="">
                <div class="contact-card-line-text"><a href="tel:+4318906401-0"><span class="text-span-5">+43 1 890 64 01-0</span></a></div>
            </div>
              <div class="contact-card-line"><img src="images/at-icon_1at-icon.png" width="24" alt="">
                <div class="contact-card-line-text"><a href="mailto:wien@dialogschmiede.com"><span class="text-span-3">wien@dialogschmiede.com</span></a></div>
              </div>
            </div>
          </div>
        </div>
        <div class="side-feature-media">
          <div class="contact-card">
            <div class="contact-card-info">
              <h5 class="heading-3">Berlin</h5>
              <div class="contact-card-line"><img src="images/pin-icon_1pin-icon.png" width="24" alt="">
                <div class="contact-card-line-text">Isoldestraße 2<br>12159 Berlin<br>Deutschland</div>
              </div>
              <div class="contact-card-line"><img src="images/phone-icon_1phone-icon.png" width="24" alt="">
                <div class="contact-card-line-text"><a href="tel:+493029673755"><span class="text-span-4">+49 30 296 737 55</span></a></div>
              </div>
              <div class="contact-card-line"><img src="images/at-icon_1at-icon.png" width="24" alt="">
                <div class="contact-card-line-text"><a href="mailto:berlin@dialogschmiede.com" class="link">berlin@dialogschmiede.com</a></div>
              </div>
            </div>
          </div>
        </div>
        <div class="side-feature-media">
          <div class="contact-card">
            <div class="contact-card-info">
              <h5 class="heading-4">Zürich</h5>
              <div class="contact-card-line"><img src="images/pin-icon_1pin-icon.png" width="24" alt="">
                <div class="contact-card-line-text">Gutschstrasse 7<br>6313 Menzingen<br>Schweiz</div>
              </div>
              <div class="contact-card-line"><img src="images/phone-icon_1phone-icon.png" width="24" alt="">
                <div class="contact-card-line-text"><a href="tel:+41417550259" class="link-2"><span>+41 41 755 02 59</span></a></div>
              </div>
              <div class="contact-card-line"><img src="images/at-icon_1at-icon.png" width="24" alt="">
                <div class="contact-card-line-text"><a href="mailto:zuerich@dialogschmiede.com"><span class="text-span-2">zuerich@dialogschmiede.com</span></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="vertical-header"></div>
    </div>
    <div class="section grey-section no-top-padding no-bottom-padding">
      <div class="wrapper w-container">
        <div class="footer">
          <div class="footer-about"><a href="/" class="footer-logo w-nav-brand w--current"><img src="images/dialogschmiede_logo_1.png" srcset="images/dialogschmiede_logo_1-p-500.png 500w, images/dialogschmiede_logo_1.png 600w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 42vw, (max-width: 991px) 27vw, 20vw" alt=""></a>
            <p class="paragraph-small">Die Dialogschmiede ist Österreichs <br>führende Dialogagentur.</p>
          </div>
          <div class="footer-nav">
            <h5>Navigation</h5><a href="/" class="footer-link w--current">Home</a><a href="portfolio" class="footer-link">Cases</a><a href="news" class="footer-link">News</a><a href="team" class="footer-link">Team</a><a href="kontakt" class="footer-link">Kontakt</a></div>
          <div class="footer-subscribe">
            <!-- <h5>Jetzt unseren Newsletter abonnieren:</h5>
            <div class="w-form">
              <form id="wf-form-Subscribe-Form" name="wf-form-Subscribe-Form" data-name="Subscribe Form" class="footer-subscribe-form"><input type="hidden" name="Message" value="Newsletter Subscription"><input type="email" id="email-4" name="Email" data-name="Email 4" placeholder="Ihre E-Mail Adresse" maxlength="256" required="" class="input footer-input w-input"><input type="submit" value="Abonnieren" data-wait="Please wait..." class="button w-button"></form>
              <div class="form-success w-form-done">
                <div>Vielen Dank! Wir haben Ihre Nachricht erhalten und melden uns bald.</div>
              </div>
              <div class="form-error w-form-fail">
                <div>Oops! Da ist etwas schiefgelaufen. Bitte versuchen Sie es erneut.</div>
              </div>
            </div> -->
            <?php include "footer.inc.php"; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="overlay">
 </div>
  <div class="overlay_box">
  <div class="x">x</div>
  <center><img src="/images/popup_dsgvo_anim_neu.gif" style="width:90%;" /></center><br>
              <!-- <p class="grey-text">Liebe DIALOGSCHMIEDE Kunden, Freunde und Partner,<br><br>
aufgrund der aktuellen Situation haben wir uns vorgestern Abend dazu entschlossen, unseren Betrieb bis auf weiteres auf remote und Homeoffice umzustellen. Damit möchten wir ein klares Statement gegen das Coronavirus setzen, OHNE EINSCHRÄNKUNG unserer Leistungen für Kunden und Partner. Durch unsere Organisationsstruktur und technischen Lösungen hatten wir die Möglichkeit, diesen drastischen Schritt rasch umzusetzen.
<br><br>
Auf diesem Wege versuchen wir, das ganze Geschehen rund um den Virus einzudämmen. Der tatsächliche Ernst der Lage wurde uns bewusst, als die WHO den Krisenstand „Pandemie“ ausgesprochen hat. Es liegt nun an jedem Einzelnen, diese verheerenden Zustände weitestgehend zu minimieren.
<br><br>
Mit diesem Schreiben möchten wir auf keinen Fall für Aufruhr sorgen, sondern lediglich an alle appellieren, selbst Teil davon zu sein & gegen den Ausbruch dieser Krankheit mitzuwirken. Sprich – Händeschütteln, Bussis, Umarmungen & der generelle soziale Kontakt (wie in Agenturen üblich) mit anderen Mitmenschen müssen momentan weitestgehend vermieden werden. Nur so können wir bald wieder zur Normalität übergehen & das Virus sich nicht weiter ausbreiten. Wenn wir uns alle an das halten, was die Regierung empfiehlt und weitestgehend „zu Hause“ bleiben, erleben wir hoffentlich nicht dieselbe Dimension wie momentan in unserem Nachbarland Italien. 
<br><br>
Wir müssen jetzt handeln & isolieren, damit das Virus nicht noch mehr unkontrollierbare Ausmaße annehmen kann, an die wir nicht im Geringsten denken möchten. Und setzen daher ein Statement. Um unsere Mitarbeiterinnen und Mitarbeiter zu schützen und dadurch auch den Betrieb und die Leistung für unsere Kunden weiter uneingeschränkt zu gewährleisten.
<br><br>

Das DIALOGSCHMIEDE Team.</p> -->
  </div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <script>
  jQuery('.overlay').click(function(){
    jQuery('.overlay').css({'display':'none'});
    jQuery('.overlay_box').css({'display':'none'});
  });
  jQuery('.overlay_box').click(function(){
    jQuery('.overlay').css({'display':'none'});
    jQuery('.overlay_box').css({'display':'none'});
  });

  function aud_play_pause() {
  var myAudio = document.getElementById("vid1");
  if (myAudio.paused) {
    myAudio.play();
  } else {
    myAudio.pause();
  }
}
  // jQuery("#vid1"). prop('muted', false); 

  </script>
  <style>
  .x {
    cursor: pointer;
    float:right;
  }
  .overlay  { 
    background-color:#fff; 
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
   
    filter:alpha(opacity=60);
    -moz-opacity:0.6;
    -khtml-opacity: 0.6;
    opacity: 0.6;
    z-index: 1000;
    display:none;
    }

  .overlay_box {
    top:200px;
    color:#777;
      position:absolute;
      z-index:1001;
      width:50%;
      height:auto;
      background-color:white;
      border: solid 1px #f7ab17;
      border-radius:20px;
      margin-left:25%;
      text-align:left;
      filter:alpha(opacity=100);
      -moz-opacity:1;
      -khtml-opacity: 1;
      opacity: 1;
      padding:15px;
      display:none;
  }
  .overlay_box img {align:center;}
  .overlay_box p {
    font-size:14px !important;
  }


  @media (max-width: 991px) {
    .overlay_box {
      width:90%;
      margin-left:5%;
      
  }

  }
  </style>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>