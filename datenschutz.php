<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Thu Jan 16 2020 10:06:47 GMT+0000 (Coordinated Universal Time)  -->
<html data-wf-page="5d63f6a81fb966c4835cded8" data-wf-site="5cea4eba7a0da81f0863712a">
<head>
  <meta charset="utf-8">
  <title>Dialogschmiede Datenschutz</title>
  <meta content="Sicher im Umgang mit Daten. In der Datenschutzerklärung finden Sie eine detaillierte Übersicht aller relevanten Punkte." name="description">
  <meta content="Dialogschmiede Agentur" property="og:title">
  <meta content="Sicher im Umgang mit Daten. In der Datenschutzerklärung finden Sie eine detaillierte Übersicht aller relevanten Punkte." property="og:description">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/dialogschmiede.webflow.css" rel="stylesheet" type="text/css">
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Lato:100,100italic,300,300italic,400,400italic,700,700italic,900,900italic"]  }});</script> -->
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="images/webclip.png" rel="apple-touch-icon">
<?php include "header.inc.php"; ?></head>
<body class="body">
  <div data-ix="preloader" class="page-loader"></div>
  <div data-ix="page-wrapper-load" class="page-wrapper">
    <div class="contact-popup">
      <div class="popup-wrapper">
        <div class="contact-popup-window w-form">
          <a href="#" data-ix="hide-contact-popup" class="minimize-icon w-inline-block">
            <div class="minimize-icon-line"></div>
          </a>
          <h3>Kontaktieren Sie uns</h3>
          <form id="wf-form-Contact-Popup-Form" name="wf-form-Contact-Popup-Form" data-name="Contact Popup Form" class="contact-window-form"><input type="text" id="Name-3" name="Name" data-name="Name" placeholder="Name" maxlength="256" class="input w-input"><input type="email" id="Email-3" name="Email" data-name="Email" placeholder="E-Mail Adresse" maxlength="256" required="" class="input w-input"><textarea id="Message" name="Message" placeholder="Wie können wir Ihnen helfen?" maxlength="5000" required="" data-name="Message" class="input text-area w-input"></textarea><input type="submit" value="Nachricht senden" data-wait="Please wait..." class="button form-button w-button"></form>
          <div class="form-success window-success w-form-done">
            <div class="text-block-16">Vielen Dank! Wir haben Ihre Nachricht erhalten und melden uns bald.</div>
          </div>
          <div class="form-error window-error w-form-fail">
            <div class="text-block-15">Oops! Da ist etwas schiefgelaufen. Bitte versuchen Sie es erneut.</div>
          </div>
        </div>
        <div data-ix="hide-contact-popup" class="popup-overlay"></div>
      </div>
    </div>
    <div data-collapse="medium" data-animation="default" data-duration="400" data-ix="nav-bar" class="nav-bar w-nav">
      <div class="wrapper w-container"><a href="/" data-ix="logo" class="logo-link w-nav-brand"><img src="images/dialogschmiede_logo_1.png" width="180" srcset="images/dialogschmiede_logo_1-p-500.png 500w, images/dialogschmiede_logo_1.png 600w" sizes="(max-width: 479px) 100vw, 180px" alt=""></a>
        <nav role="navigation" class="nav-menu w-nav-menu"><a href="portfolio" class="nav-link">Cases</a><a href="news" class="nav-link">News</a><a href="team" class="nav-link">Team</a><a href="kontakt" class="nav-link">Kontakt</a></nav>
        <div class="menu-button white-menu-button w-nav-button">
          <div class="w-icon-nav-menu" style="color:black"></div>
        </div>
      </div>
    </div>
    <div class="section textblock">
      <div class="section-impressum textblock">
        <h2 class="section-header-impressum"><strong class="bold-text-3">Datenschutz</strong></h2>
        <div class="divider left"></div>
        Stand: 26.2.2020 <br>
      <br>
              <h3>Allgemeines</h3> 
        <p class="grey-text">
        

              <br><br><strong> 1.1 Verantwortliche</strong><br>

              Die Dialogschmiede (siehe <a href="https://www.dialogschmiede.com/impressum">Impressum</a>) betreibt diese Website zum Zweck der umfassenden Information über ihre Leistungen und Services sowie als Tool für die Kontaktaufnahme und Kommunikation. Unsere weiteren Niederlassungen (siehe <a href="https://www.dialogschmiede.com/kontakt">Kontakt</a>) sind Partner und datenschutzrechtlich für die Website jeweils gemeinsam Verantwortliche mit der Dialogschmiede Wien.  

              

              <br><br><strong>1.2 Der Schutz Ihrer Daten</strong> <br>

              Wir nehmen den Schutz Ihrer personenbezogenen Daten sehr ernst. Die Einhaltung der geltenden gesetzlichen Bestimmungen über Datenschutz, das sind die EU Datenschutz-Grundverordnung (EU) 2016/679 (DSGVO) und das österreichische Datenschutzgesetz, BGBl I 165/1999 idgF, ist für uns eine Selbstverständlichkeit. Für Fragen über Datenschutz auf dieser Website steht Ihnen unsere Datenschutzbeauftragte <!--unter der Tel. + 43 1 890 64 01-0 oder -->über E-Mail <a href="mailto:datenschutz@dialogschmiede.com">datenschutz@dialogschmiede.com</a> zur Verfügung. 

              Diese Datenschutzerklärung informiert Sie, die Nutzer dieser Website über Zweck, Art, Umfang und Rechtsgrundlagen der Erhebung und Verwendung personenbezogener Daten durch die Dialogschmiede (im Folgenden auch: wir oder Betreiber). 

              

              <br><br><strong>1.3 Auftragsverarbeiter </strong><br>

              Für den Schutz Ihrer Daten bleiben wir auch bei Heranziehung von Auftragsverarbeitern verantwortlich. Mit den Auftragsverarbeitern haben wir jeweils Verträge gemäß Art 28 DSGVO zur Sicherung der Einhaltung des Datenschutzes geschlossen. Auftragsverarbeiter außerhalb der Europäischen Union werden nur eingesetzt, soweit für das betreffende Drittland ein Beschluss der Europäischen Kommission über das Vorhandensein von angemessenem Datenschutz in diesem Drittland vorliegt oder Standard-Datenschutzklauseln vereinbart wurden. 

              
              <br><br><strong>1.4 Sicherheit Ihrer personenbezogenen Daten </strong><br>
              

              Personenbezogene Daten sind alle Informationen über eine bestimmte oder identifizierbare natürliche Person („Betroffene“). Eine identifizierbare natürliche Person ist eine Person, die direkt oder indirekt identifiziert werden kann, insbesondere durch Bezugnahme auf einen Identifikator wie einen Namen, eine Identifikationsnummer, Ortsdaten, ein Online-Kennzeichen oder anhand eines oder mehrerer spezifischer Merkmale der physischen, physiologischen, genetischen, geistigen, wirtschaftlichen, kulturellen oder sozialen Identität dieser natürlichen Person. 

              Wir schützen Ihre personenbezogenen Daten vor unerlaubtem Zugriff, Verwendung oder Veröffentlichung. Wir sorgen dafür, dass Ihre personenbezogenen Daten, die von uns gespeichert werden, in einer kontrollierten sicheren Umgebung verwendet werden, die unerlaubten Zugriff und Veröffentlichung verhindert.  

              <br><strong>SSL- bzw. TLS-Verschlüsselung </strong><br>

              Diese Seite nutzt aus Sicherheitsgründen und zum Schutz der Übertragung vertraulicher Inhalte, wie zum Beispiel Bestellungen oder Anfragen, die Sie an uns als Seitenbetreiber senden, eine SSL- bzw. TLS-Verschlüsselung. Eine verschlüsselte Verbindung erkennen Sie daran, dass die Adresszeile des Browsers von „http://“ auf „https://“ wechselt und an dem Schloss-Symbol in Ihrer Browserzeile. 

              Wenn die SSL- bzw. TLS-Verschlüsselung aktiviert ist, können die Daten, die Sie an uns übermitteln, nicht von Dritten mitgelesen werden. 

              </p>
              <br><br><h3>Datenerfassung und Datennutzung </h3>
              <p class="grey-text">
              <br><strong>1.1 Wie erfassen wir Ihre Daten?</strong><br>
               

              Ihre Daten werden zum einen dadurch erhoben, dass Sie uns diese mitteilen. Hierbei kann es sich z. B. um Daten handeln, die Sie in ein Kontaktformular eingeben. 

              Andere Daten werden automatisch oder nach Ihrer Einwilligung beim Besuch der Website durch unsere IT-Systeme erfasst. Das sind vor allem technische Daten (z. B. Internetbrowser, Betriebssystem oder Uhrzeit des Seitenaufrufs). Die Erfassung dieser Daten erfolgt automatisch, sobald Sie diese Website betreten. 

              
              <br><br><strong>1.2 Wofür nutzen wir Ihre Daten und warum dürfen wir das? </strong><br>
              

              Ein Teil der Daten wird erhoben, um eine fehlerfreie Bereitstellung der Website zu gewährleisten: Wir verarbeiten bei Ihrem Besuch auf unserer Website die für die Herstellung und Aufrechterhaltung der Kommunikation notwendigen Daten, insbes. folgende Daten: Datum/Uhrzeit, Meldung über erfolgreichen Abruf, Browsertyp/Version, Betreibersystem und IP-Adresse. Rechtsgrundlage dieser Datenverarbeitung ist § 96 Abs 3, 3. Satz TKG 2003, BGBl I 70/2003 idgF: Die Verarbeitung ist erforderlich, um dem Nutzer die von ihm gewünschte Kommunikation mit der Website zu ermöglichen, und daher zulässig.  
            <br><br> 
              Andere Daten können zur Analyse und Tracking Ihres Nutzerverhaltens verwendet werden. Hierfür ist gemäß § 96 Abs. 3 TKG 2003 die Einwilligung der betroffenen Person erforderlich. Tracking findet auf dieser Website nur statt, wenn ein Website-Besucher zu dieser Verarbeitung im Consent Management Banner eingewilligt hat. Im Banner kann die Einwilligung für alle oder für ausgewählte Cookies / Technologien erteilt werden. Die auf dieser Website verfolgten Zwecke von Tracking durch Cookies / andere Technologien sind die folgenden: 
                <br> 
              Durch Cookies / Technologien ermittelte Daten verarbeiten wir auch für Website-statistische Zwecke im Zusammenhang mit der Überwachung der Funktionstüchtigkeit und Reichweite unserer Website. Dabei werden nur pseudonymisierte Daten verwendet und aggregierte Daten erzeugt (d.s. Informationen über eine größere Anzahl von Personen, z. B. „23 % der Websitebesucher kommen aus Deutschland“). Das für diesen Zweck von uns eingesetzte Programm ist Matomo. 
              <br>
              Interessenserkundung bei den Nutzern der Website durch Beobachtung ihres Surf- und Klickverhaltens, um unsere Informationen nach diesen Interessen auszurichten und dadurch den Informationsnutzen unserer Website zu erhöhen. Hierfür wird der droidhelper eingesetzt.  
              <br>  <br>
              Daten aus den Kontaktformularen helfen uns, Ihre Anfragen zu bearbeiten:  
                <br>
              Sie haben die Möglichkeit über ein Formular mit uns in Kontakt zu treten. Die von Ihnen bereitgestellten Daten (Name, E-Mail-Adresse, Nachricht) werden für die Bearbeitung der Anfrage und für den Fall von Anschlussfragen gespeichert. Rechtsgrundlage dieser Datenverarbeitung ist die konkludente Einwilligung gemäß Art. 6 Abs. 1 lit. a DSGVO, die durch das Absenden der Nachricht gegeben wird. 
              <br>
              Bewerber haben zudem die Möglichkeit direkt über die Website Ihre Unterlagen zum Zwecke der Bewerbung zu übermitteln. Diese Datenverarbeitung erfolgt auf Grundlage von Art. 6 Abs. 1 lit. a und von Art. 6 Abs. 1 lit. f DSGVO.
              Seitenumbruch
              </p>
              <br><br><h3>Zusätzliche Informationen zu einzelnen Arten der Verarbeitung und zu eingesetzten Technologien: </h3><br>
              <p class="grey-text">
              <strong>1. Server-Log-Dateien </strong><br>
              

              Der Provider der Seiten erhebt und speichert automatisch Informationen in so genannten Server-Log Dateien, die Ihr Browser automatisch an uns übermittelt. Dies sind: 
                <br>
              Browsertyp und Browserversion 
              <br>
              verwendetes Betriebssystem 
              <br>
              Referrer URL 
              <br>
              Hostname des zugreifenden Rechners 
              <br>
              Uhrzeit der Serveranfrage 
              <br>
              IP-Adresse 
              <br>
              Eine Zusammenführung dieser Daten mit anderen Datenquellen wird nicht vorgenommen. 

              Die Erfassung dieser Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an der technisch fehlerfreien Darstellung und der Optimierung seiner Website – hierzu müssen die Server-Log-Files erfasst werden. 
              <br><br><strong>2. Cookies / Technologien</strong><br>
               

              Unsere Internetseiten verwenden so genannte „Cookies“. Cookies sind kleine Textdateien und richten auf Ihrem Endgerät keinen Schaden an. Sie werden entweder vorübergehend für die Dauer einer Sitzung (Session-Cookies) oder dauerhaft (permanente Cookies) auf Ihrem Endgerät gespeichert. Session-Cookies werden nach Ende Ihres Besuchs automatisch gelöscht. Permanente Cookies bleiben auf Ihrem Endgerät gespeichert bis Sie diese selbst löschen oder eine automatische Lösung durch Ihren Webbrowser erfolgt.  

              
              <br><br><strong>2.1 Zweck von Cookies</strong><br>
               

              Der Zweck von Cookies ist unterschiedlich, z.T. sind sie notwendig, um eine elektronische Kommunikation aufrechtzuerhalten (sog. „Session Cookies“) , z.T. dienen sie der Überprüfung der Funktionsfähigkeit und Reichweite der Website („Analyse-Cookies“), z.T. können sie die Nutzung der Website für den Besucher einfacher (z. B. indem frühere Nutzereinstellungen wiederhergestellt werden) und zielgerichteter (z. B. durch Personalisierung) machen. Je nach dem unterschiedlichen Zweck von Cookies ist auch die erforderliche Rechtsgrundlage unterschiedlich. 

              
              <br><br><strong>2.2. Rechtsgrundlagen von Cookies</strong><br>
               

              Cookies, die zur Durchführung des elektronischen Kommunikationsvorgangs (notwendige Cookies) oder zur Bereitstellung bestimmter, von Ihnen erwünschter Funktionen (funktionale Cookies, z. B. für das Bewerbermanagement) oder zur Optimierung der Website (z. B. Cookies zur Messung des Webpublikums) erforderlich sind, werden auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO gespeichert, sofern keine andere Rechtsgrundlage angegeben wird. Der Websitebetreiber hat ein berechtigtes Interesse an der Speicherung von Cookies zur technisch fehlerfreien und optimierten Bereitstellung seiner Dienste.  

              

              Sofern eine Einwilligung zur Speicherung von Cookies / Technologien abgefragt wurde, erfolgt die Speicherung der betreffenden Cookies / Technologien ausschließlich auf Grundlage dieser Einwilligung (Art. 6 Abs. 1 lit. a DSGVO); die Einwilligung ist jederzeit über das sogenannten Consent Management (Symbol auf der linken unteren Ecke) widerrufbar. 

              

              Wir weisen in diesem Zusammenhang darauf hin, dass in die Cookie-Nutzung und die daraus folgende Verarbeitung personenbezogener Daten nur solche Personen einwilligen können, die das vierzehnte Lebensjahr vollendet haben. Für Personen, die jünger sind als vierzehn, muss die Einwilligung durch den Erziehungsberechtigten vorgenommen werden. 

              
              <br><br><strong>2.3 Information über Unterdrückung von Cookies </strong><br>
              

              Eine Nutzung unserer Website ist auch ohne Komfortfunktionen von Cookies möglich. Sie können in Ihrem Browser das Speichern von Cookies generell deaktivieren, auf bestimmte Websites beschränken oder Ihren Browser so einstellen, dass Sie benachrichtigt werden, bevor ein Cookie gespeichert wird. Sie können die Cookies über die Datenschutzfunktion Ihres Browsers auch jederzeit von der Festplatte des Endgerätes löschen. Bei einer Deaktivierung der Cookies kann die Funktion und die Benutzerfreundlichkeit der Website allerdings eingeschränkt sein. 

              Informationen zur Deaktivierung von Cookies von beispielsweise Drittanbietern finden Sie unter anderem auf den folgenden Websiten: <a href="https://optout.networkadvertising.org/?c=1">https://optout.networkadvertising.org/?c=1</a> und <a href="https://www.youronlinechoices.com/at/">https://www.youronlinechoices.com/at/</a>

              
              <br><br><strong>2.4 Cookie-Einwilligung mit Usercentrics </strong><br>
              

              Diese Website nutzt die Cookie-Consent-Technologie von Usercentrics, um Ihre Einwilligung zur Speicherung bestimmter Cookies auf Ihrem Endgerät einzuholen und diese datenschutzkonform zu dokumentieren. Anbieter dieser Technologie ist die Usercentrics GmbH, Rosental 4, 80331 München, Website: <a href="https://usercentrics.com/de/">https://usercentrics.com/de/</a> (im Folgenden „Usercentrics“). 

              Wenn Sie unsere Website betreten, werden folgende personenbezogene Daten an Usercentrics übertragen: 
              <br>
              Ihre Einwilligung(en) bzw. der Widerruf Ihrer Einwilligung(en) 
              <br>
              Ihre IP-Adresse 
              <br>
              Informationen über Ihren Browser 
              <br>
              Informationen über Ihr Endgerät 
              <br>
              Zeitpunkt Ihres Besuchs auf der Website 
              <br>
              Des Weiteren speichert Usercentrics ein Cookie in Ihrem Browser, um Ihnen die erteilten Einwilligungen bzw. deren Widerruf zuordnen zu können. Die so erfassten Daten werden gespeichert, bis Sie uns zur Löschung auffordern, das Usercentrics-Cookie selbst löschen oder der Zweck für die Datenspeicherung entfällt. Zwingende gesetzliche Aufbewahrungspflichten bleiben unberührt. 

              Der Einsatz von Usercentrics erfolgt, um die gesetzlich vorgeschriebenen Einwilligungen für den Einsatz von Cookies einzuholen. Rechtsgrundlage hierfür ist Art. 6 Abs. 1 S. 1 lit. c DSGVO. 

              
              <br><br><strong>2.5 Implementierte Technologien   </strong><br>
              

              Diese Website setzt folgende Technologien ein: 
              <br>
              Usercentrics Consent Management Platform <br>
              <div class="uc-embed" uc-layout="privacySettings" style="border:solid 1px #ccc;"></div>
              <p class="grey-text">
              Matomo (ehemals Piwik) 
              <br>
              <!--Google analytics 
              <br-->
              Prescreens 
              <br>
              Vimeo 
              <br>
              Droid 
              <br><br>
              <!-- Die Informationen zu den jeweiligen Technologien können Sie durch Klick auf das Symbol links unten jederzeit abrufen.  -->
              <!--strong>Google Analytics</strong>
              <ul style="list-style:none;" class="grey-text">    
              <li>
              <br>
              Diese Website nutzt Funktionen des Webanalysedienstes Google Analytics. Anbieter ist die Google Ireland Limited („Google“), Gordon House, Barrow Street, Dublin 4, Irland.
              <br>

              Google Analytics verwendet so genannte „Cookies“. Das sind Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert.
              <br>
              Die Speicherung von Google-Analytics-Cookies und die Nutzung dieses Analyse-Tools erfolgen auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.
              <br><br>
              </li>
              <li>
              <strong>IP Anonymisierung</strong>
              <br>
              Wir haben auf dieser Website die Funktion IP-Anonymisierung aktiviert. Dadurch wird Ihre IP-Adresse von Google innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum vor der Übermittlung in die USA gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.
              <br>
              <br>
              </li><li>
                <strong>Browser Plugin</strong>
              <br>
              Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch den Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem Sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren: https://tools.google.com/dlpage/gaoptout?hl=de
              <br>
              <br>
              </li><li>
                <strong>Widerspruch gegen Datenerfassung</strong>
              <br>
              Sie können die Erfassung Ihrer Daten durch Google Analytics verhindern, indem Sie im Consent Management Ihre Einwilligung deaktivieren.
              <br>
              Mehr Informationen zum Umgang mit Nutzerdaten bei Google Analytics finden Sie in der Datenschutzerklärung von Google: https://support.google.com/analytics/answer/6004245?hl=de
              </li>
              </ul-->
              <p class="grey-text">
              <br><br><strong>3. Kontaktformular </strong><br>
              

              Wenn Sie uns per Kontaktformular Anfragen zukommen lassen, werden Ihre Angaben aus dem Anfrageformular inklusive der von Ihnen dort angegebenen Kontaktdaten zwecks Bearbeitung der Anfrage und für den Fall von Anschlussfragen bei uns gespeichert. Diese Daten geben wir nicht ohne Ihre Einwilligung weiter. 

              Die Verarbeitung dieser Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. b DSGVO, sofern Ihre Anfrage mit der Erfüllung eines Vertrags zusammenhängt oder zur Durchführung vorvertraglicher Maßnahmen erforderlich ist. In allen übrigen Fällen beruht die Verarbeitung auf unserem berechtigten Interesse an der effektiven Bearbeitung der an uns gerichteten Anfragen (Art. 6 Abs. 1 lit. f DSGVO) oder auf Ihrer Einwilligung (Art. 6 Abs. 1 lit. a DSGVO) sofern diese abgefragt wurde. 

              Die von Ihnen im Kontaktformular eingegebenen Daten verbleiben bei uns, bis Sie uns zur Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen (per E-Mail an <a href="mailto:datenschutz@dialogschmiede.com">datenschutz@dialogschmiede.com</a>) oder der Zweck für die Datenspeicherung entfällt (z. B. nach abgeschlossener Bearbeitung Ihrer Anfrage). Zwingende gesetzliche Bestimmungen – insbesondere Aufbewahrungsfristen – bleiben unberührt.  

              
              <br><br><strong>4. Anfrage per E-Mail, Telefon oder Telefax</strong><br>
               

              Wenn Sie uns per E-Mail, Telefon oder Telefax kontaktieren, wird Ihre Anfrage inklusive aller daraus hervorgehenden personenbezogenen Daten (Name, Anfrage) zum Zwecke der Bearbeitung Ihres Anliegens bei uns gespeichert und verarbeitet. Diese Daten geben wir nicht ohne Ihre Einwilligung weiter. 

              Die Verarbeitung dieser Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. b DSGVO, sofern Ihre Anfrage mit der Erfüllung eines Vertrags zusammenhängt oder zur Durchführung vorvertraglicher Maßnahmen erforderlich ist. In allen übrigen Fällen beruht die Verarbeitung auf unserem berechtigten Interesse an der effektiven Bearbeitung der an uns gerichteten Anfragen (Art. 6 Abs. 1 lit. f DSGVO) oder auf Ihrer Einwilligung (Art. 6 Abs. 1 lit. a DSGVO) sofern diese abgefragt wurde. 

              Die von Ihnen an uns per Kontaktanfragen übersandten Daten verbleiben bei uns, bis Sie uns zur Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen (per E-Mail an <a href="mailto:datenschutz@dialogschmiede.com">datenschutz@dialogschmiede.com</a>) oder der Zweck für die Datenspeicherung entfällt (z. B. nach abgeschlossener Bearbeitung Ihres Anliegens). Zwingende gesetzliche Bestimmungen – insbesondere gesetzliche Aufbewahrungsfristen – bleiben unberührt. 
              <br><br><strong>5. Bewerberdaten </strong>
              
          <br><strong>5.1 Umgang mit Bewerberdaten</strong><br>
               

              Wir bieten Ihnen die Möglichkeit, sich bei uns zu bewerben (z. B. per E-Mail, postalisch oder via Online-Bewerberformular unter <a href="https://www.dialogschmiede.com/team">Team</a>). Im Folgenden informieren wir Sie über Umfang, Zweck und Verwendung Ihrer im Rahmen des Bewerbungsprozesses erhobenen personenbezogenen Daten. Wir versichern, dass die Erhebung, Verarbeitung und Nutzung Ihrer Daten in Übereinstimmung mit geltendem Datenschutzrecht und allen weiteren gesetzlichen Bestimmungen erfolgt und Ihre Daten streng vertraulich behandelt werden. 

              
                <br><br><strong>5.2 Umfang und Zweck der Datenerhebung </strong><br>
              

              Wenn Sie uns eine Bewerbung zukommen lassen, verarbeiten wir Ihre damit verbundenen personenbezogenen Daten (z. B. Kontakt- und Kommunikationsdaten, Bewerbungsunterlagen, Notizen im Rahmen von Bewerbungsgesprächen etc.), soweit dies zur Entscheidung über die Begründung eines Beschäftigungsverhältnisses erforderlich ist. Rechtsgrundlage hierfür ist Art. 6 Abs. 1 lit. b DSGVO (allgemeine Vertragsanbahnung) und – sofern Sie eine Einwilligung erteilt haben – Art. 6 Abs. 1 lit. a DSGVO. Die Einwilligung ist jederzeit widerrufbar. Ihre personenbezogenen Daten werden innerhalb unseres Unternehmens ausschließlich an Personen weitergegeben, die an der Bearbeitung Ihrer Bewerbung beteiligt sind. 

              

              Sofern die Bewerbung erfolgreich ist, werden die von Ihnen eingereichten Daten auf Grundlage von Art. 6 Abs. 1 lit. b DSGVO zum Zwecke der Durchführung des Beschäftigungsverhältnisses in unseren Datenverarbeitungssystemen gespeichert. 

              
              <br><br><strong>5.3 Aufbewahrungsdauer der Daten</strong><br>
               

              Sofern wir Ihnen kein Stellenangebot machen können, Sie ein Stellenangebot ablehnen oder Ihre Bewerbung zurückziehen, behalten wir uns das Recht vor, die von Ihnen übermittelten Daten auf Grundlage unserer berechtigten Interessen (Art. 6 Abs. 1 lit. f DSGVO) bis zu 6 Monate ab der Beendigung des Bewerbungsverfahrens (Ablehnung oder Zurückziehung der Bewerbung) bei uns aufzubewahren. Anschließend werden die Daten gelöscht und die physischen Bewerbungsunterlagen vernichtet. Die Aufbewahrung dient insbesondere Nachweiszwecken im Falle eines Rechtsstreits. Sofern ersichtlich ist, dass die Daten nach Ablauf der 6-Monatsfrist erforderlich sein werden (z. B. aufgrund eines drohenden oder anhängigen Rechtsstreits), findet eine Löschung erst statt, wenn der Zweck für die weitergehende Aufbewahrung entfällt. 

              

              Eine längere Aufbewahrung kann außerdem stattfinden, wenn Sie eine entsprechende Einwilligung (Art. 6 Abs. 1 lit. a DSGVO) erteilt haben oder wenn gesetzliche Aufbewahrungspflichten der Löschung entgegenstehen. 

              
              <br><br><strong>5.4 Aufnahme in den Bewerber-Pool</strong><br>
               

              Sofern wir Ihnen kein Stellenangebot machen, besteht ggf. die Möglichkeit, Sie in unseren Bewerber-Pool aufzunehmen. Im Falle der Aufnahme werden alle Dokumente und Angaben aus der Bewerbung in den Bewerber-Pool übernommen, um Sie im Falle von passenden Vakanzen zu kontaktieren. 

              

              Die Aufnahme in den Bewerber-Pool geschieht ausschließlich auf Grundlage Ihrer ausdrücklichen Einwilligung (Art. 6 Abs. 1 lit. a DSGVO). Die Abgabe der Einwilligung ist freiwillig und steht in keinem Bezug zum laufenden Bewerbungsverfahren. Der Betroffene kann seine Einwilligung jederzeit widerrufen. In diesem Falle werden die Daten aus dem Bewerber-Pool unwiderruflich gelöscht, sofern keine gesetzlichen Aufbewahrungsgründe vorliegen. Die Daten aus dem Bewerber-Pool werden spätestens nach einem Jahr nach Erteilung der Einwilligung unwiderruflich gelöscht. 

              
              <br><br><strong>5.5 Bewerbermanagementtool von Prescreen  </strong><br>
              

              Wir haben auf unserer Website das Bewerbermanagementtool von Prescreen eingebunden. Das Tool ermöglicht es Ihnen gleich direkt über die Website Ihre Bewerbungsunterlagen anzulegen und zu übermitteln. Anbieter dieses Tools ist die Prescreen International GmbH, Mariahilfer Straße 17, 1060, Website: <a href="https://prescreen.io/de/">https://prescreen.io/de/</a> (im Folgenden „Prescreen“). 

              Für die automatisierte Verarbeitung Ihrer Bewerbung werden folgende Daten im Tool erhoben und verarbeitet: 
              <br>
              Vorname, Nachname, E-Mail und ggf. auch Adresse/Ort, Geburtsdatum, Anrede, Telefonnummer, Staatsbürgerschaft 
              <br>
              Von der jeweiligen Ausschreibung abhängige Zusatzfragen (z. B. Führerschein) 
              <br>
              Lebenslauf, insbesondere Angaben zu Berufserfahrung und Ausbildung 
              <br>
              Kompetenzen (z. B. Photoshop, MS Office) 
              <br>
              Bewerbungsfoto 
              <br>
              Qualifikationen, Auszeichnungen und Sprachfähigkeiten 
              <br>
              Motivationsschreiben 
              <br>
              Dateien und Dokumente, die Sie ggf. hochladen 
              <br>
              Zudem wird die schriftliche, elektronische Kommunikation, die zwischen Ihnen und dem Unternehmen (Dialogschmiede Wien, Dialogschmiede Berlin, Dialogschmiede Zürich) für das Sie sich bewerben, stattfindet gespeichert. Des Weiteren verarbeiten wir Kommentare und Bewertungen, die im Zuge Ihres Bewerbungsprozesses zu Ihnen verfasst werden. 

              Prescreen erfasst im Tool auch statistische Informationen Ihres Surf-Verhaltens auf (z. B. welche Bereiche Sie anklicken oder Ihre "Log-in"-Zeitpunkte). Diese Daten können zum Zwecke des Supports oder der Service-Optimierung aufgezeichnet und bei Bedarf jederzeit durch den Prescreen-Administrator eingesehen werden. 

              Prescreen verarbeitet und nutzt die erhobenen, personenbezogenen Daten zur Erbringung der Prescreen-Dienste, zum Zwecke der Registrierung auf der Website der Dialogschmiede und zum Zwecke des Austauschs von Informationen zwischen Bewerbern und Unternehmen. Die Speicherfristen decken sich mit den unter Punkt 5.3 genannten Fristen. 

              Rechtsgrundlage ist Art. 6 Abs. 1 lit. a DSGVO. 

              
              <br><br><strong>6. Unsere Social–Media–Auftritte </strong><br><strong>6.1 Datenverarbeitung durch soziale Netzwerke</strong><br>
               

              Wir unterhalten öffentlich zugängliche Profile in sozialen Netzwerken. Die im Einzelnen von uns genutzten sozialen Netzwerke finden Sie weiter unten. 

              

              Soziale Netzwerke wie Facebook, Twitter etc. können Ihr Nutzerverhalten in der Regel umfassend analysieren, wenn Sie deren Website oder eine Website mit integrierten Social-Media-Inhalten (z. B. Like-Buttons oder Werbebannern) besuchen. Durch den Besuch unserer Social-Media-Präsenzen werden zahlreiche datenschutzrelevante Verarbeitungsvorgänge ausgelöst.  

              

              Im Einzelnen: <br>

              Wenn Sie in Ihrem Social-Media-Account eingeloggt sind und unsere Social-Media-Präsenz besuchen, kann der Betreiber des Social-Media-Portals diesen Besuch Ihrem Benutzerkonto zuordnen. Ihre personenbezogenen Daten können unter Umständen aber auch dann erfasst werden, wenn Sie nicht eingeloggt sind oder keinen Account beim jeweiligen Social-Media-Portal besitzen. Diese Datenerfassung erfolgt in diesem Fall beispielsweise über Cookies, die auf Ihrem Endgerät gespeichert werden oder durch Erfassung Ihrer IP-Adresse. 

              Mit Hilfe der so erfassten Daten können die Betreiber der Social-Media-Portale Nutzerprofile erstellen, in denen Ihre Präferenzen und Interessen hinterlegt sind. Auf diese Weise kann Ihnen interessenbezogene Werbung in- und außerhalb der jeweiligen Social-Media-Präsenz angezeigt werden. Sofern Sie über einen Account beim jeweiligen sozialen Netzwerk verfügen, kann die interessenbezogene Werbung auf allen Geräten angezeigt werden, auf denen Sie eingeloggt sind oder eingeloggt waren. 

              Bitte beachten Sie außerdem, dass wir nicht alle Verarbeitungsprozesse auf den Social-Media-Portalen nachvollziehen können. Je nach Anbieter können daher ggf. weitere Verarbeitungsvorgänge von den Betreibern der Social-Media-Portale durchgeführt werden. Details hierzu entnehmen Sie den Nutzungsbedingungen und Datenschutzbestimmungen der jeweiligen Social-Media-Portale. 

              

              
              <br><br><strong>6.2 Rechtsgrundlage </strong><br>
              

              Unsere Social-Media-Auftritte sollen eine möglichst umfassende Präsenz im Internet gewährleisten. Hierbei handelt es sich um ein berechtigtes Interesse im Sinne von Art. 6 Abs. 1 lit. f DSGVO. Die von den sozialen Netzwerken initiierten Analyseprozesse beruhen ggf. auf abweichenden Rechtsgrundlagen, die von den Betreibern der sozialen Netzwerke anzugeben sind (z. B. Einwilligung im Sinne des Art. 6 Abs. 1 lit. a DSGVO). 

              
              <br><br><strong> 6.3 Verantwortlicher und Geltendmachung von Rechten </strong><br>
             

              Wenn Sie einen unserer Social-Media-Auftritte (z. B. Facebook) besuchen, sind wir gemeinsam mit dem Betreiber der Social-Media-Plattform für die bei diesem Besuch ausgelösten Datenverarbeitungsvorgänge verantwortlich. Sie können Ihre Rechte (Auskunft, Berichtigung, Löschung, Einschränkung der Verarbeitung, Datenübertragbarkeit und Beschwerde) grundsätzlich sowohl ggü. uns als auch ggü. dem Betreiber des jeweiligen Social-Media-Portals (z. B. ggü. Facebook) geltend machen. 

              

              Bitte beachten Sie, dass wir trotz der gemeinsamen Verantwortlichkeit mit den Social-Media-Portal-Betreibern nicht vollumfänglich Einfluss auf die Datenverarbeitungsvorgänge der Social-Media-Portale haben. Unsere Möglichkeiten richten sich maßgeblich nach der Unternehmenspolitik des jeweiligen Anbieters. 

              
              <br><br><strong>6.4 Speicherdauer </strong><br>
              

              Die unmittelbar von uns über die Social-Media-Präsenz erfassten Daten werden von unseren Systemen gelöscht, sobald der Zweck für ihre Speicherung entfällt, Sie uns zur Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen oder der Zweck für die Datenspeicherung entfällt. Gespeicherte Cookies verbleiben auf Ihrem Endgerät, bis Sie sie löschen. Zwingende gesetzliche Bestimmungen – insb. Aufbewahrungsfristen – bleiben unberührt. 

              

              Auf die Speicherdauer Ihrer Daten, die von den Betreibern der sozialen Netzwerke zu eigenen Zwecken gespeichert werden, haben wir keinen Einfluss. Für Einzelheiten dazu informieren Sie sich bitte direkt bei den Betreibern der sozialen Netzwerke (z. B. in deren Datenschutzerklärung, siehe unten). 

              
              <br><br><strong>6.5 Soziale Netzwerke im Einzelnen</strong><br>
               
              <strong>Facebook</strong><br>
               

              Wir verfügen über ein Profil bei Facebook. Anbieter dieses Dienstes ist die Facebook Ireland Limited, 4 Grand Canal Square, Dublin 2, Irland. Die erfassten Daten werden nach Aussage von Facebook auch in die USA und in andere Drittländer übertragen. 

              Sie können Ihre Werbeeinstellungen selbstständig in Ihrem Nutzer-Account anpassen. Klicken Sie hierzu auf folgenden Link und loggen Sie sich ein: <a href="https://www.facebook.com/settings?tab=ads">https://www.facebook.com/settings?tab=ads</a>. 

              Details entnehmen Sie der Datenschutzerklärung von Facebook: <a href="https://www.facebook.com/about/privacy/">https://www.facebook.com/about/privacy/</a>. 

              
              <br><br><strong>Twitter</strong><br>
               

              Wir nutzen den Kurznachrichtendienst Twitter. Anbieter ist die Twitter International Company, One Cumberland Place, Fenian Street, Dublin 2, D02 AX07, Irland.
              Sie können Ihre Twitter-Datenschutzeinstellungen selbstständig in Ihrem Nutzer-Account anpassen. Klicken Sie hierzu auf folgenden Link und loggen Sie sich ein: <a href="https://twitter.com/personalization">https://twitter.com/personalization</a>. 

              Details entnehmen Sie der Datenschutzerklärung von Twitter: <a href="https://twitter.com/de/privacy">https://twitter.com/de/privacy</a>. 

              
              <br><br><strong>XING</strong><br>
               

              Wir verfügen über ein Profil bei XING. Anbieter ist die XING AG, Dammtorstraße 29-32, 20354 Hamburg, Deutschland. Details zu deren Umgang mit Ihren personenbezogenen Daten entnehmen Sie der Datenschutzerklärung von XING: <a href="https://privacy.xing.com/de/datenschutzerklaerung">https://privacy.xing.com/de/datenschutzerklaerung</a>. 

              
              <br><br><strong>LinkedIn</strong><br>
               

              Wir verfügen über ein Profil bei LinkedIn. Anbieter ist die LinkedIn Ireland Unlimited Company, Wilton Plaza, Wilton Place, Dublin 2, Irland. LinkedIn verwendet Werbecookies. 

              Wenn Sie LinkedIn-Werbe-Cookies deaktivieren möchten, nutzen Sie bitte folgenden Link: <a href="https://www.linkedin.com/psettings/guest-controls/retargeting-opt-out">https://www.linkedin.com/psettings/guest-controls/retargeting-opt-out</a>. 

              Details zu deren Umgang mit Ihren personenbezogenen Daten entnehmen Sie der Datenschutzerklärung von LinkedIn: <a href="https://www.linkedin.com/legal/privacy-policy">https://www.linkedin.com/legal/privacy-policy</a>. 
              <br><br><strong>Instagram</strong><br>

              Wir verfügen über ein Profil bei Instagram. Anbieter ist die Instagram Inc., 1601 Willow Road, Menlo Park, CA, 94025, USA. Details zu deren Umgang mit Ihren personenbezogenen Daten entnehmen Sie der Datenschutzerklärung von Instagram: <a href="https://help.instagram.com/519522125107875" target="_blank">https://help.instagram.com/519522125107875</a>.

              <br><br><strong>Vimeo</strong><br>

              Wir verfügen über ein Profil bei Vimeo. Anbieter ist die Vimeo, Inc., 555 West 18th Street, New York 10011, USA. Details zu deren Umgang mit Ihren personenbezogenen Daten entnehmen Sie der Datenschutzerklärung von Vimeo: <a href="https://vimeo.com/privacy" target="_blank">https://vimeo.com/privacy</a>.
              </p>
              <h3>Ihre Rechte gegenüber dem für die Datenverarbeitung Verantwortlichen:  </h3>
              <p class="grey-text">
             
              Nach Art 15 DSGVO haben Sie ein <b>Recht auf Auskunft</b> darüber, ob und welche Daten über Sie von uns verarbeitet werden.  
              <br>
              Nach Art 16 DSGVO steht Ihnen das <b>Recht auf Berichtigung</b> fehlerhafter oder unvollständiger Daten zu.  
              <br>
              Weiters haben Sie im Umfang des Art 17 ein <b>Recht auf Löschung</b> Ihrer Daten, falls sie unrechtmäßigerweise verarbeitet werden. 
              <br>
              Ist die Rechtmäßigkeit oder Richtigkeit von Daten strittig, kann gemäß Art 18 die <b>Einschränkung der Verarbeitung</b> durch den Verantwortlichen verlangt werden, damit Daten ¬– bis zur Streitentscheidung – nicht weiterverwendet und insbesondere nicht verändert werden 
              <br><br>
              <b>Recht auf Datenübertragung  </b>
              <br>
              Art 20 verschafft das Recht, zum Zweck der Datenübertragung vom Verantwortlichen die Herausgabe der ihm vom Betroffenen zur Verfügung gestellten Daten in einem gängigen maschinenlesbarer Format zu verlangen, sofern die Verarbeitung auf Grund einer Einwilligung oder eines Vertrages erfolgt. 
              <br><br>
              <b>Widerspruchsrecht </b>
              <br>
              Nach Art 21 kann gegen Verarbeitungen, die sich auf Art 6 (1)(e) [gesetzlich übertragene Aufgabe] oder (f) [berechtigte Interessen] stützen, Widerspruch erhoben werden, wenn die Verarbeitung nach Auffassung des Widersprechenden seine überwiegenden schutzwürdigen Interessen verletzt.  
              <br> <br>
              <b>Geltendmachung Ihrer Rechte </b>
              <br>
              Um eines der genannten Rechte uns gegenüber geltend zu machen, kontaktieren Sie uns bitte einfach per Mail an die <a href="mailto:datenschutz@dialogschmiede.com">datenschutz@dialogschmiede.com</a>  
              <br>
              Bestätigung Ihrer Identität: Zum Schutz Ihrer Rechte und Ihrer Privatsphäre sind wir berechtigt im Zweifel einen Identitätsnachweis anzufordern. 
              <br>
              Wenn Sie eines der genannten Rechte besonders häufig oder nach der Sachlage offenkundig unbegründet einfordern, sind wir berechtigt ein angemessenes Bearbeitungsentgelt zu verlangen oder die Bearbeitung des Antrages abzulehnen. 
              <br>
              Verstößt Ihres Erachtens, die Verarbeitung Ihrer personenbezogenen Daten gegen geltendes Datenschutzrecht, so kontaktieren Sie uns bitte, um allfällige Fragen zu klären, an die Adresse <a href="mailto:datenschutz@dialogschmiede.com">datenschutz@dialogschmiede.com</a>  
              <br> <br>
              <b>Weitere Informationen </b>
              <br>
              Ihr Vertrauen ist uns wichtig. Daher möchten wir Ihnen jederzeit Rede und Antwort bezüglich der Verarbeitung Ihrer personenbezogenen Daten stehen. Wenn Sie Fragen haben, die Ihnen diese Datenschutzerklärung nicht beantworten konnte oder wenn Sie zu einem Punkt vertiefte Informationen wünschen, wenden Sie sich bitte direkt an unser Team unter <a href="mailto:datenschutz@dialogschmiede.com">datenschutz@dialogschmiede.com</a>  Wir möchten Ihnen im Falle von Auskunftsersuchen, Anregungen oder bei Beschwerden als Ansprechpartner zur Verfügung stehen. 
              <br> <br>
              <b>Beschwerderecht an die Datenschutzbehörde </b>
              <br>
              Sie haben weiters das Recht auf Beschwerde an die österreichische Datenschutzbehörde (Barichgasse 40-42 1030 Wien, <a href="mailto:dsb@dsb.gv.at">dsb@dsb.gv.at</a>, <a href="https://www.dsb.gv.at">https://www.dsb.gv.at</a>), wenn der Betreiber dieser Website nicht innerhalb eines Monats auf Ihre Anfrage reagiert.   
              <br><br>
              <b>Datenschutzbeauftragte </b>
              <br>
              Der Betreiber dieser Website hat eine Datenschutzbeauftragte bestellt.               <br>
              Diese kann unter 
              <br>
              Dialogschmiede GmbH               <br>
              Celine Polterauer               <br>
              Ungargasse 64-66/Stg. 1/Top 110               <br>
              1030 Wien               <br>
              <br>
              <!-- Tel.: + 43 1 890 64 01-0               <br> -->
              e-Mail: <a href="mailto:datenschutz@dialogschmiede.com">datenschutz@dialogschmiede.com</a>                 <br>

              kontaktiert werden. 
        </p>
      </div>
    </div>
    <div class="section grey-section no-top-padding no-bottom-padding">
      <div class="wrapper w-container">
        <div class="footer">
          <div class="footer-about"><a href="/" class="footer-logo w-nav-brand"><img src="images/dialogschmiede_logo_1.png" srcset="images/dialogschmiede_logo_1-p-500.png 500w, images/dialogschmiede_logo_1.png 600w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 42vw, (max-width: 991px) 27vw, 20vw" alt=""></a>
            <p class="paragraph-small">Die Dialogschmiede ist Österreichs <br>führende Dialogagentur.</p>
          </div>
          <div class="footer-nav">
            <h5>Navigation</h5><a href="/" class="footer-link">Home</a><a href="portfolio" class="footer-link">Cases</a><a href="news" class="footer-link">News</a><a href="team" class="footer-link">Team</a><a href="kontakt" class="footer-link">Kontakt</a></div>
          <div class="footer-subscribe">
            <!-- <h5>Jetzt unseren Newsletter abonnieren:</h5>
            <div class="w-form">
              <form id="wf-form-Subscribe-Form" name="wf-form-Subscribe-Form" data-name="Subscribe Form" class="footer-subscribe-form"><input type="email" id="email-4" name="email-4" data-name="Email 4" placeholder="Ihre E-Mail Adresse" maxlength="256" required="" class="input footer-input w-input"><input type="submit" value="Abonnieren" data-wait="Please wait..." class="button w-button"></form>
              <div class="form-success w-form-done">
                <div>Vielen Dank! Wir haben Ihre Nachricht erhalten und melden uns bald.</div>
              </div>
              <div class="form-error w-form-fail">
                <div>Oops! Da ist etwas schiefgelaufen. Bitte versuchen Sie es erneut.</div>
              </div>
            </div>-->
 <?php include "footer.inc.php"; ?>
        </div>
      </div>
    </div>
  </div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>